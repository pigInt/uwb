using System;
using System.Security.Cryptography;
using System.Text;

namespace AppCore
{
	/// <summary>
	/// 包含各类常见的加解密算法的工具类
	/// </summary>
	public class EncryptAndDecrypt
	{
		#region Constructor
        public EncryptAndDecrypt()
		{
			//
			// TODO: 在此处添加构造函数逻辑
			//
		}
		#endregion

		#region 单向散列算法

		#region 采用SHA1对字符进行加密后返回(BASE64格式)
		/// <summary>
		/// 采用SHA1对字符进行加密后返回(BASE64格式)
		/// </summary>
		/// <param name="CryptString">加密的字符串</param>
		/// <param name="DecryptString">加密后的字符串(BASE64格式)</param>
		/// <returns>是否加密成功</returns>
		public bool CryptSHA1ToBase64(string CryptString,out string DecryptString)
		{
			DecryptString="";
			try
			{
				SHA1 sha=new SHA1CryptoServiceProvider();
				byte[] fromData = System.Text.Encoding.UTF8.GetBytes(CryptString);
				byte[] targetData = sha.ComputeHash(fromData);
				DecryptString=Convert.ToBase64String(targetData);
				return true;
			}
			catch(Exception e)
			{
				ErrorLog(e);
				return false;
			}

		}
		#endregion

		#region 采用MD5对字符进行加密后返回(BASE64格式)
		/// <summary>
		/// 采用MD5对字符进行加密后返回(BASE64格式)
		/// </summary>
		/// <param name="CryptString">加密的字符串</param>
		/// <param name="DecryptString">加密后的字符串(BASE64格式)</param>
		/// <returns>是否加密成功</returns>
		public bool CryptMD5ToBase64(string CryptString,out string DecryptString)
		{
			DecryptString="";
			try
			{
				MD5 md5=new MD5CryptoServiceProvider();
				byte[] fromData = System.Text.Encoding.UTF8.GetBytes(CryptString);
				byte[] targetData = md5.ComputeHash(fromData);
				DecryptString=Convert.ToBase64String(targetData);
				return true;
			}
			catch(Exception e)
			{
				ErrorLog(e);
				return false;
			}
		}		
		#endregion

		#region 采用MD5对字符进行加密后返回(HEX格式)
		/// <summary>
		/// 采用MD5对字符进行加密后返回(HEX格式)
		/// </summary>
		/// <param name="password"></param>
		/// <returns></returns>
		public string CryptMD5ToHEX(string CryptString)
		{
			byte[] Bytes = System.Text.Encoding.ASCII.GetBytes(CryptString); 
			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] result = md5.ComputeHash(Bytes);

			string MD5Result = "";
			foreach(byte r in result)
			{
				MD5Result += Uri.HexEscape((char)r).Substring(1,2);
			}
			return MD5Result;
		}
		#endregion

		#endregion

		#region 对称加密算法

		#region 采用3DES对加密字符串进行加密并以BASE64格式返加密串.
		/// <summary>
		/// 采用3DES对加密字符串进行加密并以BASE64格式返加密串.
		/// </summary>
		/// <param name="CryptString">需要加密的字符串</param>
		/// <param name="Key">密钥</param>
		/// <param name="DecryptString">加密完后的字符串(以BASE64格式返回)</param>
		/// <returns>加密是否成功</returns>
		public bool Crypt3DESToBase64(string CryptString,string Key,out string DecryptString)
		{
			DecryptString="";
			try
			{
				//把字符串编码成字节数组
				byte[] KEY=HexStringToByteArray(Key);
				byte[] CRYPTSTRING=System.Text.Encoding.UTF8.GetBytes(CryptString);

				//设置KEY值及IV值
				byte[] tmpiv={1,2,3,4,5,6,7,8};
				byte[] tmpkey={0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7};
				//只取前24个字符(3DES规定)
				for(int ii=0;ii<24;ii++)
				{
					tmpkey[ii]=KEY[ii];
				}

				//设置3DES加密模式等
				TripleDESCryptoServiceProvider dsp =new TripleDESCryptoServiceProvider();
				dsp.Mode= System.Security.Cryptography.CipherMode.CBC ;
				dsp.Padding = System.Security.Cryptography.PaddingMode.PKCS7 ;

				//建立加密后的ICryptoTransform对象
				ICryptoTransform tridesencrypt = dsp.CreateEncryptor(tmpkey,tmpiv);

				//取出加密后的字符串
				DecryptString=Convert.ToBase64String(tridesencrypt.TransformFinalBlock( CRYPTSTRING,0,CRYPTSTRING.Length));

				dsp.Clear();

				return true;
			}
			catch(Exception e)
			{
				ErrorLog(e);
				return false;
			}
		}
		#endregion

		#region 采用3DES对加密字符串进行加密并以BASE64格式返加密串.(固定密钥)
		/// <summary>
		/// 采用3DES对加密字符串进行加密并以BASE64格式返加密串.
		/// </summary>
		/// <param name="CryptString">需要加密的字符串</param>
		/// <param name="DecryptString">加密完后的字符串(以BASE64格式返回)</param>
		/// <returns>加密是否成功</returns>
		public bool Crypt3DESToBase64(string CryptString,out string DecryptString)
		{
			string Key="FC64332F412EAA1BA8E98011C06504C19B9C5BCEB94DB708";
			DecryptString="";
			try
			{

				//把字符串编码成字节数组
				byte[] KEY=System.Text.Encoding.UTF8.GetBytes(Key);
				byte[] CRYPTSTRING=System.Text.Encoding.UTF8.GetBytes(CryptString);

				//设置KEY值及IV值
				//byte[] tmpiv ={ 0 };
				// 针对错误：“指定的初始化向量(IV)与此算法的块大小不匹配。”进行修改 By 林传勇 2006-10-11
				byte[] tmpiv ={ 1, 2, 3, 4, 5, 6, 7, 8 };
				byte[] tmpkey ={ 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7 };
				//只取前24个字符(3DES规定)
				for(int ii=0;ii<24;ii++)
				{
					tmpkey[ii]=KEY[ii];
				}

				//设置3DES加密模式等
				TripleDESCryptoServiceProvider  dsp =new TripleDESCryptoServiceProvider ();
				dsp.Mode= System.Security.Cryptography.CipherMode.CBC ;
				dsp.Padding = System.Security.Cryptography.PaddingMode.PKCS7 ;

				//建立加密后的ICryptoTransform对象
				ICryptoTransform tridesencrypt = dsp.CreateEncryptor(tmpkey,tmpiv);

				//取出加密后的字符串
				DecryptString=Convert.ToBase64String(tridesencrypt.TransformFinalBlock( CRYPTSTRING,0,CRYPTSTRING.Length));

				dsp.Clear();

				return true;
			}
			catch(Exception e)
			{
				ErrorLog(e);
				return false;
			}
		}
		#endregion

		#region 采用3DES对解密串(Base64格式)进行解密
		/// <summary>
		/// 采用3DES对解密串(Base64格式)进行解密
		/// </summary>
		/// <param name="DecryptString">解密串(BASE64格式)</param>
		/// <param name="Key">密钥</param>
		/// <param name="CryptString">解密后字符串</param>
		/// <returns>是否解密成功</returns>
		public bool DeCrypt3DESFromBase64(string DecryptString,string Key,out string CryptString)
		{
			CryptString="";
			try
			{

				//把字符串编码成字节数组
				byte[] KEY=HexStringToByteArray(Key);
				byte[] DECRYPTSTRING=Convert.FromBase64String(DecryptString);

				//设置KEY值及IV值
				byte[] tmpiv={1,2,3,4,5,6,7,8};
				byte[] tmpkey={0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7};
				//只取前24位(3DES规定)
				for(int ii=0;ii<24;ii++)
				{
					tmpkey[ii]=KEY[ii];
				}

				//设置3DES加密模式等
				TripleDESCryptoServiceProvider  dsp =new TripleDESCryptoServiceProvider ();
				dsp.Mode= System.Security.Cryptography.CipherMode.CBC ;
				dsp.Padding = System.Security.Cryptography.PaddingMode.PKCS7 ;

				ICryptoTransform tridesencrypt = dsp.CreateDecryptor(tmpkey,tmpiv);
				CryptString=System.Text.Encoding.UTF8.GetString(tridesencrypt.TransformFinalBlock( DECRYPTSTRING,0,DECRYPTSTRING.Length));

				dsp.Clear();
				return true;
			}
			catch(Exception e)
			{
				ErrorLog(e);
				return false;
			}
		}
		#endregion

		#region 采用3DES对解密串(Base64格式)进行解密(固定密钥)
		/// <summary>
		/// 采用3DES对解密串(Base64格式)进行解密
		/// </summary>
		/// <param name="DecryptString">解密串(BASE64格式)</param>
		/// <param name="CryptString">解密后字符串</param>
		/// <returns>是否解密成功</returns>
		public bool DeCrypt3DESFromBase64(string DecryptString,out string CryptString)
		{
			string Key="FC64332F412EAA1BA8E98011C06504C19B9C5BCEB94DB708";
			CryptString="";
			try
			{

				//把字符串编码成字节数组
				byte[] KEY=System.Text.Encoding.UTF8.GetBytes(Key);
				byte[] DECRYPTSTRING=Convert.FromBase64String(DecryptString);

				//设置KEY值及IV值
				//byte[] tmpiv ={ 0 };
				// 针对错误：“指定的初始化向量(IV)与此算法的块大小不匹配。”进行修改 By 林传勇 2006-10-11
				byte[] tmpiv ={ 1, 2, 3, 4, 5, 6, 7, 8 };
				byte[] tmpkey ={ 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7 };
				//只取前24位(3DES规定)
				for(int ii=0;ii<24;ii++)
				{
					tmpkey[ii]=KEY[ii];
				}

				//设置3DES加密模式等
				TripleDESCryptoServiceProvider  dsp =new TripleDESCryptoServiceProvider ();
				dsp.Mode= System.Security.Cryptography.CipherMode.CBC ;
				dsp.Padding = System.Security.Cryptography.PaddingMode.PKCS7 ;

				ICryptoTransform tridesencrypt = dsp.CreateDecryptor(tmpkey,tmpiv);
				CryptString=System.Text.Encoding.UTF8.GetString(tridesencrypt.TransformFinalBlock( DECRYPTSTRING,0,DECRYPTSTRING.Length));

				dsp.Clear();
				return true;
			}
			catch(Exception e)
			{
				ErrorLog(e);
				return false;
			}
		}
		#endregion

		#region 采用DES对加密字符串进行加密并以BASE64格式返加密串.
		/// <summary>
		/// 采用DES对加密字符串进行加密并以BASE64格式返加密串.
		/// </summary>
		/// <param name="CryptString">需要加密的字符串</param>
		/// <param name="Key">密钥</param>
		/// <param name="DecryptString">加密完后的字符串(以BASE64格式返回)</param>
		/// <returns>加密是否成功</returns>
		public bool CryptDESToBase64(string CryptString,string Key,out string DecryptString)
		{
			DecryptString="";
			try
			{

				//把字符串编码成字节数组
				byte[] KEY=System.Text.Encoding.UTF8.GetBytes(Key);
				byte[] CRYPTSTRING=System.Text.Encoding.UTF8.GetBytes(CryptString);

				//设置KEY值及IV值
				byte[] tmpiv={1,2,3,4,5,6,7,8};
				byte[] tmpkey={0,1,2,3,4,5,6,7};
				//只取前8个字符(DES规定)
				for(int ii=0;ii<8;ii++)
				{
					tmpkey[ii]=KEY[ii];
				}


				//设置DES加密模式等
				DESCryptoServiceProvider  dsp =new DESCryptoServiceProvider ();
				dsp.Mode= System.Security.Cryptography.CipherMode.CBC ;
				dsp.Padding = System.Security.Cryptography.PaddingMode.PKCS7 ;


				//建立加密后的ICryptoTransform对象
				ICryptoTransform tridesencrypt = dsp.CreateEncryptor(tmpkey,tmpiv);

				//取出加密后的字符串
				DecryptString=Convert.ToBase64String(tridesencrypt.TransformFinalBlock( CRYPTSTRING,0,CRYPTSTRING.Length));

				dsp.Clear();

				return true;
			}
			catch(Exception e)
			{
				ErrorLog(e);
				return false;
			}
		}
		#endregion

		#region 采用DES对解密串(Base64格式)进行解密
		/// <summary>
		/// 采用DES对解密串(Base64格式)进行解密
		/// </summary>
		/// <param name="DecryptString">解密串(BASE64格式)</param>
		/// <param name="Key">密钥</param>
		/// <param name="CryptString">解密后字符串</param>
		/// <returns>是否解密成功</returns>
		public bool DeCryptDESFromBase64(string DecryptString,string Key,out string CryptString)
		{
			CryptString="";
			try
			{

				//把字符串编码成字节数组
				byte[] KEY=System.Text.Encoding.UTF8.GetBytes(Key);
				byte[] DECRYPTSTRING=Convert.FromBase64String(DecryptString);

				//设置KEY值及IV值
				byte[] tmpiv={1,2,3,4,5,6,7,8};
				byte[] tmpkey={0,1,2,3,4,5,6,7};
				//只取前8个字符(DES规定)
				for(int ii=0;ii<8;ii++)
				{
					tmpkey[ii]=KEY[ii];
				}

				//设置3DES加密模式等
				DESCryptoServiceProvider  dsp =new DESCryptoServiceProvider ();
				dsp.Mode= System.Security.Cryptography.CipherMode.CBC ;
				dsp.Padding = System.Security.Cryptography.PaddingMode.PKCS7 ;

				ICryptoTransform tridesencrypt = dsp.CreateDecryptor(tmpkey,tmpiv);
				CryptString=System.Text.Encoding.UTF8.GetString(tridesencrypt.TransformFinalBlock( DECRYPTSTRING,0,DECRYPTSTRING.Length));

				dsp.Clear();
				return true;
			}
			catch(Exception e)
			{
				ErrorLog(e);
				return false;
			}
		}
		#endregion

		#endregion

		#region Authenticator相关

		#region 生成Authenticator,算法为先SHA-1,再3DES
		/// <summary>
		/// 生成Authenticator,算法为先SHA-1,再3DES
		/// </summary>
		/// <param name="key">密钥</param>
		/// <param name="source">原始数据</param>
		/// <returns>加密生成的Authenticator</returns>
		public static string GenerateAuthenticator(string key, string source)
		{
			byte[] keyArray , ivArray , inArray , outArray ;
			string strHash = "" , authenticator = "" ;

			try
			{
				// 获取加密向量和SP密钥
				ivArray = new byte[]{1,2,3,4,5,6,7,8};
				keyArray = Cryptogram.HexStringToByteArray( key );

				// 生成验证码
				strHash = Cryptogram.ComputeHashString( source );
				inArray = Cryptogram.FromBase64String( strHash );
				if ( Cryptogram.Encrypt( keyArray , ivArray , inArray , out outArray))
					authenticator = Cryptogram.ToBase64String( outArray );
			}
			catch( Exception e)
			{
				ErrorLogStatic(e);
			}

			return authenticator;
		}
		#endregion

		#region 判断Authenticator是否正确
		/// <summary>
		/// 判断Authenticator是否正确
		/// </summary>
		/// <param name="key">密钥</param>
		/// <param name="source">源串</param>
		/// <param name="authenticator">待判断的认证码</param>
		/// <returns></returns>
		public static bool ValidateAuthenticator(string key, string concatSourceString, string generatedAuthenticator)
		{
			string encryptedAuthenticator = GenerateAuthenticator(key,concatSourceString);
			return encryptedAuthenticator.Equals(generatedAuthenticator);
		}
		#endregion

		#endregion

		#region 密钥和向量相关
		/// <summary>
		/// 随机密钥生成器
		/// </summary>
		/// <returns>密钥HEX字符串</returns>
		public string GenerateKey()
		{
			byte[] byteArr = GenerateKeyByteArr();
			return Cryptogram.ByteArrayToHexString(byteArr);
		}

		/// <summary>
		/// 产生随机密钥字节数组
		/// </summary>
		/// <returns></returns>
		private byte[] GenerateKeyByteArr()
		{
			byte[] buf=null;
			try
			{
				TripleDESCryptoServiceProvider tDES = new TripleDESCryptoServiceProvider();
				tDES.GenerateKey();
				buf = tDES.Key;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return buf;
		}

		/// <summary>
		/// 生成随机加密向量
		/// </summary>
		/// <returns></returns>
		public string GenerateIV()
		{
			byte[] byteArr = GenerateIVByteArr();
			return Cryptogram.ByteArrayToHexString(byteArr);
		}

		/// <summary>
		/// 生成加密向量
		/// </summary>
		/// <returns></returns>
		private byte[] GenerateIVByteArr()
		{
			byte[] buf=null;
			try
			{
				TripleDESCryptoServiceProvider tDES = new TripleDESCryptoServiceProvider();
				tDES.GenerateIV();
				buf = tDES.IV;
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return buf;
		}
		#endregion

		#region 16进制字符转为字节数组
		private byte[] HexStringToByteArray(string s)
		{
			return Cryptogram.HexStringToByteArray(s);
		}
		#endregion

		#region 错误日志记录方法
		/// <summary>
		/// 错误日志记录方法
		/// </summary>
		/// <param name="e"></param>
		private void ErrorLog(Exception e)
		{
			//ASPLog.WriteTextLog(string.Empty,ASPLogType.ASPCommonLog,"CommonLib -> CryptographyTools",e.Message+e.StackTrace);
		}

		/// <summary>
		/// 错误日志记录方法(Static)
		/// </summary>
		/// <param name="e"></param>
		private static void ErrorLogStatic(Exception e)
		{
			//ASPLog.WriteTextLog(string.Empty,ASPLogType.ASPCommonLog,"CommonLib -> CryptographyTools",e.Message+e.StackTrace);
		}
		#endregion
	}

	#region 加解密算法类及类型转换
	/// <summary>
	/// 加解密算法类及类型转换
	/// </summary>
	public class Cryptogram
	{
		private static readonly byte[] pKEY={ 218,239,227,22,31,53,120,224,223,223,171,210,140,158,47,86,122,39,238,95,47,138,44,155};
		private static readonly byte[] pIV ={1,2,3,4,5,6,7,8};
		
		private static TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();

		static Cryptogram()
		{
			des.Mode= System.Security.Cryptography.CipherMode.CBC ;
			des.Padding = System.Security.Cryptography.PaddingMode.PKCS7 ;			
		}

		public static bool Encrypt(byte[] KEY ,byte[] IV ,byte[] TobeEncrypted, out  byte[] Encrypted )
		{
			Encrypted=null;
			try
			{
				byte[] tmpiv={0,1,2,3,4,5,6,7};
				for(int ii=0;ii<8;ii++)
				{
					tmpiv[ii]=IV[ii];
				}
				byte[] tmpkey={0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7};
				for(int ii=0;ii<24;ii++)
				{
					tmpkey[ii]=KEY[ii];
				}
				//tridesencrypt.Dispose();
				ICryptoTransform tridesencrypt = des.CreateEncryptor(tmpkey,tmpiv);
				//tridesencrypt = des.CreateEncryptor(KEY,tmpiv);
				Encrypted = tridesencrypt.TransformFinalBlock( TobeEncrypted,0,TobeEncrypted.Length);
				//tridesencrypt.Dispose();
				des.Clear();
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return true;
		}

		public static bool Decrypt(byte[] KEY ,byte[] IV,byte[] TobeDecrypted,out  byte[] Decrypted )
		{
			Decrypted=null;
			try
			{
				byte[] tmpiv={0,1,2,3,4,5,6,7};
				for(int ii=0;ii<8;ii++)
				{
					tmpiv[ii]=IV[ii];
				}
				byte[] tmpkey={0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7};
				for(int ii=0;ii<24;ii++)
				{
					tmpkey[ii]=KEY[ii];
				}
				//tridesdecrypt.Dispose();				
				ICryptoTransform tridesdecrypt = des.CreateDecryptor(tmpkey,tmpiv);
				//tridesdecrypt = des.CreateDecryptor(KEY,tmpiv);
				Decrypted = tridesdecrypt.TransformFinalBlock(TobeDecrypted,0,TobeDecrypted.Length );
				//tridesencrypt.Dispose();
				des.Clear();
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return true;
		}
		public static string ComputeHashString(string s) 
		{
			return ToBase64String(ComputeHash(ConvertStringToByteArray(s)));
		}
		public static byte[] ComputeHash(byte[] buf)
		{
			return ((HashAlgorithm)CryptoConfig.CreateFromName("SHA1")).ComputeHash(buf);
		}
		public static string ToBase64String(byte[] buf)
		{
			return System.Convert.ToBase64String(buf);
		}
		public static byte[] FromBase64String(string s)
		{
			return System.Convert.FromBase64String(s);
		}
		public static byte[] ConvertStringToByteArray(String s)
		{
			return System.Text.Encoding.GetEncoding("utf-8").GetBytes(s);
		}
		public static string ConvertByteArrayToString(byte[] buf)
		{
			return System.Text.Encoding.GetEncoding("utf-8").GetString(buf);
		}
		public static string ByteArrayToHexString(byte[] buf)
		{
			StringBuilder sb=new StringBuilder();
			for(int i=0;i<buf.Length;i++)
			{
				sb.Append(buf[i].ToString("X").Length==2 ? buf[i].ToString("X"):"0"+buf[i].ToString("X"));
			}
			return sb.ToString();
		}
		public static byte[] HexStringToByteArray(string s)
		{
			Byte[] buf=new byte[s.Length/2];
			for(int i=0;i<buf.Length;i++)
			{
				buf[i]=(byte)(chr2hex(s.Substring(i*2,1))*0x10+chr2hex(s.Substring(i*2+1,1)));
			}
			return buf;
		}
		private static byte chr2hex(string chr)
		{
			switch(chr)
			{
				case "0":
					return 0x00;
				case "1":
					return 0x01;
				case "2":
					return 0x02;
				case "3":
					return 0x03;
				case "4":
					return 0x04;
				case "5":
					return 0x05;
				case "6":
					return 0x06;
				case "7":
					return 0x07;
				case "8":
					return 0x08;
				case "9":
					return 0x09;
				case "A":
					return 0x0a;
				case "B":
					return 0x0b;
				case "C":
					return 0x0c;
				case "D":
					return 0x0d;
				case "E":
					return 0x0e;
				case "F":
					return 0x0f;
			}
			return 0x00;
		}
	}
	#endregion
}

