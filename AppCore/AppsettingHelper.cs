﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
namespace AppCore
{
    /// <summary>
    /// 读取配置文件
    /// </summary>
    public class AppsettingHelper
    {
        public static IConfiguration Configuration { get; set; }
        static AppsettingHelper()
        {
            //ReloadOnChange = true 当appsettings.json被修改时重新加载            
            Configuration = new ConfigurationBuilder()
            .Add(new JsonConfigurationSource { Path = "appsettings.json", ReloadOnChange = true })
            .Build();
        }
    }

}
