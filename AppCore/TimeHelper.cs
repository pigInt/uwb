﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppCore
{
    /// <summary>
    /// 时间帮助类
    /// </summary>
    public static class TimeHelper
    {

        /// <summary>
        /// 时间戳(从1970年01月01日00时00分00秒开始的毫秒数)
        /// </summary>
        public static string TimeStamp
        {
            get
            {
                //1970年01月01日00时00分00秒
                System.DateTime t = new System.DateTime(1970, 1, 1, 0, 0, 0);
                //与当前的时间差
                System.TimeSpan span = System.DateTime.Now - t;
                //取刻度数(以毫秒为刻度)
                return Convert.ToString(span.Ticks / 10000);
            }
        }

        /// <summary>
        /// 时间戳(从1970年01月01日00时00分00秒开始的毫秒数)
        /// </summary>
        public static Int64 Stamp
        {
            get
            {
                //1970年01月01日00时00分00秒
                System.DateTime t = new System.DateTime(1970, 1, 1, 0, 0, 0);
                //与当前的时间差
                System.TimeSpan span = System.DateTime.Now - t;
                //取刻度数(以毫秒为刻度)
                return Convert.ToInt64(span.Ticks / 10000);
            }
        }


        /// <summary>
        /// 标准的日期时间表示法
        /// 格式为yyyy-MM-dd HH:mm:ss，其中HH取值为00-23
        /// </summary>
        public static string RegularDateTime
        {
            get
            {
                string TimeInString = string.Empty;
                int hour = DateTime.Now.Hour;
                int min = DateTime.Now.Minute;
                int sec = DateTime.Now.Second;
                int year = DateTime.Now.Year;
                int month = DateTime.Now.Month;
                int day = DateTime.Now.Day;
                string time = DateTime.Now.TimeOfDay.ToString();
                string date = DateTime.Now.Date.ToString();
                TimeInString = (year < 10) ? "0" + year.ToString() : year.ToString();
                TimeInString += "-" + ((month < 10) ? "0" + month.ToString() : month.ToString());
                TimeInString += "-" + ((day < 10) ? "0" + day.ToString() : day.ToString());
                TimeInString += " " + ((hour < 10) ? "0" + hour.ToString() : hour.ToString());
                TimeInString += ":" + ((min < 10) ? "0" + min.ToString() : min.ToString());
                TimeInString += ":" + ((sec < 10) ? "0" + sec.ToString() : sec.ToString());
                return TimeInString;
            }
        }

        /// <summary>
        /// 将指定值加到指定的时间刻度，并转换为时间戳格式
        /// </summary>
        /// <param name="dt">时间刻度</param>
        /// <param name="Type">操作类型</param>
        /// <param name="addValue">值</param>
        /// <returns></returns>
        public static string TimeStampAdd(DateTime dt, string type, double addValue)
        {
            //1970年01月01日00时00分00秒
            System.DateTime t = new System.DateTime(1970, 1, 1, 0, 0, 0);

            if (type.ToUpper() == "HOUR")
                dt = dt.AddHours(addValue);
            else if (type.ToUpper() == "DAY")
                dt = dt.AddDays(addValue);
            else if (type.ToUpper() == "MONTH")
                dt = dt.AddMonths((int)addValue);
            else if (type.ToUpper() == "YEAR")
                dt = dt.AddYears((int)addValue);
            else
                return "";

            System.TimeSpan span = dt - t;
            //取刻度数(以毫秒为刻度)
            return Convert.ToString(span.Ticks / 10000);
        }

        /// <summary>
        /// 将时间戳转化为标准时间
        /// </summary>
        /// <param name="TimeStamp">从1970年01月01日00时00分00秒开始的毫秒数</param>
        /// <returns>标准时间</returns>
        public static DateTime ConvertToDateTime(string TimeStamp)
        {
            //1970年01月01日00时00分00秒
            System.DateTime t = new System.DateTime(1970, 1, 1, 0, 0, 0);
            System.TimeSpan span = new System.TimeSpan(Convert.ToInt64(TimeStamp) * 10000);
            return t + span;
        }

        /// <summary>
        /// 将日期时间类转换为时间戳
        /// </summary>
        /// <param name="date">标准日期时间类System.DateTime</param>
        /// <returns></returns>
        public static string ConvertToTimeStamp(System.DateTime date)
        {
            //1970年01月01日00时00分00秒
            System.DateTime t = new System.DateTime(1970, 1, 1, 0, 0, 0);
            //与当前的时间差
            System.TimeSpan span = date - t;
            //取刻度数(以毫秒为刻度)
            return Convert.ToString(span.Ticks / 10000);
        }

        /// <summary>
        /// 将日期时间字符串转换为时间戳
        /// </summary>
        /// <param name="datetime">日期字符串 如2004-9-9 23:59:59</param>
        /// <returns></returns>
        public static string ConvertToTimeStamp(string datetime)
        {
            System.DateTime t;
            t = System.DateTime.Parse(datetime);
            return ConvertToTimeStamp(t);
        }
    }


    [Serializable]
    public struct ShortTime
    {
        public int Hour;
        public int Minute;
        public int Second;

        public static ShortTime TimeParse(string timeInt)
        {
            //不足6位补0
            string timeIntStr = timeInt;
            if (timeInt.Length < 6)
            {
                for (int i = 0; i < 6 - timeInt.Length; i++)
                {
                    timeIntStr = "0" + timeIntStr;
                }
            }
            ShortTime objTime = new ShortTime();
            int startIndex = (timeIntStr.Length - 2) >= 0 ? (timeIntStr.Length - 2) : 0;
            int length = (timeIntStr.Length - 2) >= 0 ? 2 : timeIntStr.Length;
            objTime.Second = int.Parse(timeIntStr.Substring(startIndex, length));
            timeIntStr = timeIntStr.Remove(startIndex, length);
            startIndex = (timeIntStr.Length - 2) >= 0 ? (timeIntStr.Length - 2) : 0;
            length = (timeIntStr.Length - 2) >= 0 ? 2 : timeIntStr.Length;
            objTime.Minute = int.Parse(timeIntStr.Substring(startIndex, length));
            timeIntStr = timeIntStr.Remove(startIndex, length);
            objTime.Hour = int.Parse(timeIntStr);
            return objTime;
        }

        public ShortTime(int paramHour, int paramMinute, int paramSecond)
        {
            Hour = paramHour;
            Minute = paramMinute;
            Second = paramSecond;
        }

        public bool Isnull()
        {
            if (Hour == 0 && Minute == 0 && Second == 0)
            { return true; }
            return false;
        }

        public int TimeToInt()
        {
            return int.Parse(Hour.ToString() + Minute.ToString() + Second.ToString());
        }
    }
}
