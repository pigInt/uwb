﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload.Device;
using AppApi.ViewModels.Gateway;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;
using AutoMapper;

namespace AppApi.Extensions
{
    public class Gateway : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public Gateway(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        #region 私有方法
        /// <summary>
        /// 上传参数
        /// </summary>
        /// <param name="request">用户ID字符串,多个以逗号隔开</param>
        /// <returns></returns>
        public ResponseModel attribute_edit(attribute_model request)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (request.device_code.Trim().Length <= 0)
            {
                response.SetFailed("请输入编号");
                return response;
            }
            using (_dbContext)
            {
                var device_info = _dbContext.dnc_device.FirstOrDefault(x => x.DICode == request.device_code);
                if (device_info == null || device_info.DIid == 0)
                {
                    response.SetFailed("设备不存在");
                    return response;
                }
                List<attributes> list = request.attributes;
                if (list == null)
                {
                    response.SetFailed("设备参数为空");
                    return response;
                }
                int num = 0;
                foreach (attributes attr in list)
                {
                    var attr_model = _dbContext.dnc_device_extend.FirstOrDefault(x => x.dieid == device_info.DIid && x.monitor_id == attr.param_key);
                    if (attr_model != null)  //  存在则修改
                    {
                        attr_model.local_value = attr.param_value;
                        attr_model.mod_date = DateTime.Now.ToString();
                        attr_model.remote_value = attr.param_value;
                        _dbContext.SaveChanges();
                        num++;
                    }
                    else //增加
                    {
                        attribute_create_model model = new attribute_create_model();
                        model.diid = device_info.DIid;
                        model.dicode = device_info.DICode;
                        model.monitor_id = attr.param_key;
                        model.local_value = attr.param_value;
                        model.remote_value = attr.param_value;
                        var entity = _mapper.Map<attribute_create_model, dnc_device_extend>(model);
                        entity.diid = model.diid;
                        entity.dicode = model.dicode;
                        entity.monitor_id = model.monitor_id;
                        entity.local_value = model.local_value;
                        entity.remote_value = model.remote_value;
                        entity.create_date = DateTime.Now.ToString();
                        entity.mod_date = DateTime.Now.ToString();
                        _dbContext.dnc_device_extend.Add(entity);
                        _dbContext.SaveChanges();
                        num++;
                    }
                }
                if (num == 0)
                {
                    response.SetFailed("处理失败");
                }
                else
                {
                    response.SetFailed("处理成功");
                }
                return response;
            }
        }
        #endregion
    }
}
