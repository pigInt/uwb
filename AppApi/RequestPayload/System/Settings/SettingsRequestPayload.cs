﻿using AppEntity.Entities;
using System;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.RequestPayload.System.Settings
{
    public class SettingsRequestPayload : RequestPayload
    {     /// <summary>
          /// 是否已被删除
          /// </summary>
        public IsDeleted IsDeleted { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
    }
}
