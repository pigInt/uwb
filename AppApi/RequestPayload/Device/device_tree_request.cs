﻿using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.RequestPayload.Device
{
    public class device_tree_request
    {
        public string Kw { get; set; }
        /// <summary>
        /// 是否已被删除
        /// </summary>
        public int IsDeleted { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        public string citycode { get; set; }
        public string zonecode { get; set; }

    }
}
