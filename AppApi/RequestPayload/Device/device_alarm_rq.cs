﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.RequestPayload.Device
{
    public class device_alarm_rq : RequestPayload
    {
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
    }
}
