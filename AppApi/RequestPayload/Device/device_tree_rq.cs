﻿using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.RequestPayload.Device
{
    public class device_tree_rq
    {
        public string Kw { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        public int parent_id { get; set; }
        public string parent_name { get; set; }
    }
}
