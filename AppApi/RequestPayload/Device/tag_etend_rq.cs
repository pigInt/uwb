﻿using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.RequestPayload.Device
{
    public class tag_etend_rq
    {
        public string Kw { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        public int region_guid { get; set; }
    }
}
