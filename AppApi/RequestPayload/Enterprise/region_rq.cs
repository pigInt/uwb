﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.RequestPayload.Enterprise
{
    public class region_rq : RequestPayload
    {
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
    }
}
