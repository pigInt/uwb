﻿/**  版本信息模板在安装目录下，可自行修改。
* tag_extend.cs
*
* 功 能： N/A
* 类 名： tag_extend
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020-11-09 23:07:14   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;

namespace AppApi.RequestPayload.Gateway
{
    public class request_tag_rp
    {
        public List<device_tag_rp> request_data { get; set; }
    }

	public partial class device_tag_rp
    {
		public device_tag_rp()
		{}
		#region Model
		/// <summary>
		/// auto_increment
		/// </summary>
		public string device_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string unique_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int region_guid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal position_x { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal position_y { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal position_z { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal A0 { get; set; }
        public decimal A1 { get; set; }
        public decimal A2 { get; set; }
        public decimal A3 { get; set; }
        /// <summary>
        /// on update CURRENT_TIMESTAMP
        /// </summary>
        public string create_time { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string create_millisecond { get; set; }

        #endregion Model

    }
}

