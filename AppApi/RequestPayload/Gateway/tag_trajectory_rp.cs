﻿/**  版本信息模板在安装目录下，可自行修改。
* tag_extend.cs
*
* 功 能： N/A
* 类 名： tag_extend
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2020-11-09 23:07:14   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.RequestPayload.Gateway
{
	public partial class tag_trajectory_rp
    {
		public tag_trajectory_rp()
		{}
		#region Model
		/// <summary>
		/// auto_increment
		/// </summary>
        public string Kw { get; set; }

        public Status Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int region_guid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string start_time { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string end_time { get; set; }


        #endregion Model

    }
}

