﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.ViewModels.Enterprise
{
    public class region_edit_vm
    {
        /// <summary>
		/// auto_increment
		/// </summary>        
		public int guid { get; set; }

        /// <summary>
        /// name
        /// </summary>        
        public string name { get; set; }

        /// <summary>
        /// position
        /// </summary>        
        public string position { get; set; }

        /// <summary>
        /// length
        /// </summary>        
        public int length { get; set; }

        /// <summary>
        /// width
        /// </summary>        
        public int width { get; set; }

        /// <summary>
        /// height
        /// </summary>        
        public int height { get; set; }

        /// <summary>
        /// background
        /// </summary>        
        public string background { get; set; }
        public int background_ratio { get; set; }

        /// <summary>
        /// longitude
        /// </summary>        
        public string longitude { get; set; }

        /// <summary>
        /// latitude
        /// </summary>        
        public string latitude { get; set; }

        /// <summary>
        /// parent_id
        /// </summary>        
        public int? parent_guid { get; set; }

        public string parent_name { get; set; }
        /// <summary>
        /// create_time
        /// </summary>        
        public string create_time { get; set; }
        /// <summary>
        /// modify time
        /// </summary>        
        public string modify_time { get; set; }

    }
}
