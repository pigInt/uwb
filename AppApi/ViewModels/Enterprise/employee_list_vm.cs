﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Enterprise
{
    public class employee_list_vm
    {
        /// <summary>
        /// auto_increment
        /// </summary>        
		public string guid { get; set; }
        /// <summary>
        /// code
        /// </summary>        
        public string code { get; set; }
        /// <summary>
        /// name
        /// </summary>        
        public string name { get; set; }

        /// <summary>
        /// sex
        /// </summary>        
        public string sex { get; set; }

        /// <summary>
        /// age
        /// </summary>        
        public string age { get; set; }

        /// <summary>
        /// position
        /// </summary>        
        public string position { get; set; }

        /// <summary>
        /// department
        /// </summary>        
        public string department { get; set; }

        /// <summary>
        /// telephone
        /// </summary>        
        public string telephone { get; set; }

        /// <summary>
        /// status
        /// </summary>        
        public Status status { get; set; }

        /// <summary>
        /// create_time
        /// </summary>        
        public string create_time { get; set; }

        /// <summary>
        /// modify_time
        /// </summary>        
        public string modify_time { get; set; }
    }
}
