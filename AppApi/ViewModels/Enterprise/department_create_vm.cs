﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Enterprise
{
    public class department_create_vm
    {

        /// <summary>
        /// name
        /// </summary>        
        public string name { get; set; }

        /// <summary>
        /// parent_guid
        /// </summary>        
        public Guid? parent_guid { get; set; }

        /// <summary>
        /// parent_name
        /// </summary>        
        public string parent_name { get; set; }

    }
}
