﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Enterprise
{
    public class department_edit_vm
    {

        /// <summary>
        /// guid
        /// </summary>        
        public Guid guid { get; set; }

        /// <summary>
        /// name
        /// </summary>        
        public string name { get; set; }

        /// <summary>
        /// parent_guid
        /// </summary>        
        public Guid? parent_guid { get; set; }

        /// <summary>
        /// parent_name
        /// </summary>        
        public string parent_name { get; set; }

    }
}
