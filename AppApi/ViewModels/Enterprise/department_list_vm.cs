﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Enterprise
{
    public class department_list_vm
    {

        /// <summary>
        /// guid
        /// </summary>        
        public string guid { get; set; }

        /// <summary>
        /// name
        /// </summary>        
        public string name { get; set; }

        /// <summary>
        /// parent_guid
        /// </summary>        
        public string parent_guid { get; set; }

        /// <summary>
        /// parent_name
        /// </summary>        
        public string parent_name { get; set; }

    }
}
