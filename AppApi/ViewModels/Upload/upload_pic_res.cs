﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.ViewModels.Upload
{
    public class upload_pic_res
    {
        public string url { get; set; }

        /// <summary>
        /// name
        /// </summary>        
        public string filename { get; set; }

    }
}
