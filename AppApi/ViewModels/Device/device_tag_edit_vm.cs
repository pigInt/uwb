﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Device
{
    public class device_tag_edit_vm
    {
        /// <summary>
        /// id
        /// </summary>        
        public int id { get; set; }

        /// <summary>
        /// unique_id
        /// </summary>        
        public string unique_id { get; set; }

        /// <summary>
        /// name
        /// </summary>        
        public string name { get; set; }

        /// <summary>
        /// code
        /// </summary>        
        public string code { get; set; }

        /// <summary>
        /// status
        /// </summary>        
        public Status status { get; set; }

        /// <summary>
        /// type
        /// </summary>        
        public string type { get; set; }

        /// <summary>
        /// electricity
        /// </summary>        
        public int electricity { get; set; }

        /// <summary>
        /// modify_time
        /// </summary>        
        public string modify_time { get; set; }
    }
}
