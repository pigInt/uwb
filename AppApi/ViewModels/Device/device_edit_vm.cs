﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Device
{
    public class device_edit_vm
    {
        /// <summary>
        /// auto_increment
        /// </summary>        
        public int id { get; set; }
        /// <summary>
        /// code
        /// </summary>        
        public string code { get; set; }
        /// <summary>
        /// name
        /// </summary>        
        public string name { get; set; }
        /// <summary>
        /// type
        /// </summary>        
        public string type { get; set; }
        /// <summary>
        /// model
        /// </summary>        
        public string model { get; set; }
        /// <summary>
        /// mac_id
        /// </summary>        
        public string mac_id { get; set; }
        /// <summary>
        /// position_top
        /// </summary>
        public decimal position_top { get; set; }

        /// <summary>
        /// position_left
        /// </summary>
        public decimal position_left { get; set; }
        /// <summary>
        /// position_x
        /// </summary>        
        public decimal position_x { get; set; }
        /// <summary>
        /// position_y
        /// </summary>        
        public decimal position_y { get; set; }
        /// <summary>
        /// position_z
        /// </summary>        
        public decimal position_z { get; set; }
        /// <summary>
        /// status
        /// </summary>        
        public Status status { get; set; }
        /// <summary>
        /// modify time
        /// </summary>        
        public string modify_time { get; set; }

        /// <summary>
        /// region_guid
        /// </summary>        
        public int? region_guid { get; set; }

        public string region_name { get; set; }

        /// <summary>
        /// parent_id
        /// </summary>        
        public int parent_id { get; set; }
        public int sort_num { get; set; }

        /// <summary>
        /// parent_name
        /// </summary>        
        public string parent_name { get; set; }
    }
}
