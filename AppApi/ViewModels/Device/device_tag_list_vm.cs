﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Device
{
    public class device_tag_list_vm
    {
        public int id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// unique_id
        /// </summary>        
        public string unique_id { get; set; }

        /// <summary>
        /// name
        /// </summary>        
        public string name { get; set; }

        /// <summary>
        /// code
        /// </summary>        
        public string code { get; set; }

        public string type { get; set; }
        /// <summary>
        /// electricity
        /// </summary>        
        public int electricity { get; set; }

        /// <summary>
        /// status
        /// </summary>        
        public Status status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string create_time { get; set; }

    }
}
