﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Device
{
    public class v_tag_trajectory_list_vm
    {

        /// <summary>
        /// id
        /// </summary>        
        public int id { get; set; }

        /// <summary>
        /// unique_id
        /// </summary>        
        public string unique_id { get; set; }

        /// <summary>
        /// tag_name
        /// </summary>        
        public string tag_name { get; set; }

        /// <summary>
        /// tag_code
        /// </summary>        
        public string tag_code { get; set; }

        /// <summary>
        /// tag_type
        /// </summary>        
        public string tag_type { get; set; }

        /// <summary>
        /// region_guid
        /// </summary>        
        public int region_guid { get; set; }

        /// <summary>
        /// region_name
        /// </summary>        
        public string region_name { get; set; }

        /// <summary>
        /// device_id
        /// </summary>        
        public int device_id { get; set; }

        /// <summary>
        /// position_top
        /// </summary>        
        public decimal position_top { get; set; }

        /// <summary>
        /// position_left
        /// </summary>        
        public decimal position_left { get; set; }

        /// <summary>
        /// position_x
        /// </summary>        
        public decimal position_x { get; set; }

        /// <summary>
        /// position_y
        /// </summary>        
        public decimal position_y { get; set; }

        /// <summary>
        /// position_z
        /// </summary>        
        public decimal position_z { get; set; }

        /// <summary>
        /// status
        /// </summary>        
        public Status status { get; set; }

        /// <summary>
        /// create_time
        /// </summary>        
        public string create_time { get; set; }

        /// <summary>
        /// modify_time
        /// </summary>        
        public string create_millisecond { get; set; }


    }
}
