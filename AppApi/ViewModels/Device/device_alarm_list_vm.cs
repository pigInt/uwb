﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.ViewModels.Device
{
    public class device_alarm_list_vm
    {
        /// <summary>
		/// auto_increment
		/// </summary>        
		public int id { get; set; }

        /// <summary>
        /// device_code
        /// </summary>        
        public int device_id { get; set; }

        /// <summary>
        /// status
        /// </summary>        
        public Status status { get; set; }

        /// <summary>
        /// alarm_time
        /// </summary>        
        public string alarm_time { get; set; }


        /// <summary>
        /// alarm_level
        /// </summary>        
        public int alarm_level { get; set; }

        /// <summary>
        /// alarm_num
        /// </summary>        
        public int alarm_num { get; set; }
    }
}
