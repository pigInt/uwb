﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Device
{
    public class tag_trajectory_edit_vm
    {
        /// <summary>
		/// auto_increment
		/// </summary>        
		public int id { get; set; }

        /// <summary>
        /// unique_id
        /// </summary>        
        public string unique_id { get; set; }

        /// <summary>
        /// region_guid
        /// </summary>        
        public int region_guid { get; set; }

        /// <summary>
        /// position_top
        /// </summary>        
        public decimal position_top { get; set; }

        /// <summary>
        /// position_left
        /// </summary>        
        public decimal position_left { get; set; }

        /// <summary>
        /// position_x
        /// </summary>        
        public decimal position_x { get; set; }

        /// <summary>
        /// position_y
        /// </summary>        
        public decimal position_y { get; set; }

        /// <summary>
        /// position_z
        /// </summary>        
        public decimal position_z { get; set; }

        /// <summary>
        /// on update CURRENT_TIMESTAMP
        /// </summary>        
        public DateTime create_time { get; set; }

        /// <summary>
        /// create_millisecond
        /// </summary>        
        public string create_millisecond { get; set; }

    }
}
