﻿using System;
using System.Collections.Generic;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.ViewModels.Device.Device
{
    public class device_tree_model
    {
        /// <summary>
		/// auto_increment
		/// </summary>        
		public int DIid { get; set; }
        /// <summary>
        /// DICode
        /// </summary>        
        public string DICode { get; set; }

        /// <summary>
        /// DIName
        /// </summary>        
        public string DIName { get; set; }
        /// <summary>
        /// DItype
        /// </summary>        
        public string DItype { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>        
        public DateTime CreateDate { get; set; }

        public string parent_code { get; set; }

        /// <summary>
        /// Status
        /// </summary>        
        public int Status { get; set; }

        /// <summary>
        /// IsDeleted
        /// </summary>        
        public int IsDeleted { get; set; }

        /// <summary>
        /// citycode
        /// </summary>        
        public string citycode { get; set; }
        /// <summary>
        /// zonecode
        /// </summary>        
        public string zonecode { get; set; }

        /// <summary>
        /// 是否展开子节点
        /// </summary>
        public bool Expand { get; set; }
        /// <summary>
        /// 禁掉 checkbox
        /// </summary>
        public bool DisableCheckbox { get; set; }
        /// <summary>
        /// 是否选中子节点	
        /// </summary>
        public bool Selected { get; set; }
        /// <summary>
        /// 是否勾选(如果勾选，子节点也会全部勾选)
        /// </summary>
        public bool Checked { get; set; }
        public List<device_tree_model> Children { get; set; }

   

    }
}
