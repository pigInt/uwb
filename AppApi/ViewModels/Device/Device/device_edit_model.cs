﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.ViewModels.Device.Device
{
    public class device_edit_model
    {
        /// <summary>
        /// auto_increment
        /// </summary>        
        public int DIid { get; set; }
        /// <summary>
        /// DICode
        /// </summary>        
        public string DICode { get; set; }

        /// <summary>
        /// DIName
        /// </summary>        
        public string DIName { get; set; }
        /// <summary>
        /// DItype
        /// </summary>        
        public string DItype { get; set; }

        public string parent_code { get; set; }

        public int Status { get; set; }
        /// <summary>
        /// citycode
        /// </summary>        
        public string citycode { get; set; }
        /// <summary>
        /// zonecode
        /// </summary>        
        public string zonecode { get; set; }
    }
}
