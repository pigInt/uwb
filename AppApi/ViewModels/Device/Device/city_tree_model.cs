﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.ViewModels.Device.Device
{
    /// <summary>
    /// 用于角色权限的菜单树
    /// </summary>
    public class city_tree_model
    {
        /// <summary>
        /// 
        /// </summary>
        public city_tree_model()
        {

        }
        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 标题(菜单名称)
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 是否展开子节点
        /// </summary>
        public bool Expand { get; set; }
        /// <summary>
        /// 禁掉响应
        /// </summary>
        public bool Disabled { get; set; }
        /// <summary>
        /// 禁掉 checkbox
        /// </summary>
        public bool DisableCheckbox { get; set; }
        /// <summary>
        /// 是否选中子节点	
        /// </summary>
        public bool Selected { get; set; }
        /// <summary>
        /// 是否勾选(如果勾选，子节点也会全部勾选)
        /// </summary>
        public bool Checked { get; set; }
        /// <summary>
        /// 子节点属性数组
        /// </summary>
        public List<city_tree_model> Children { get; set; }
    }
}
