﻿using System;
using System.Collections.Generic;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.ViewModels.Device.Device
{
    public class device_parent_model
    {
        /// <summary>
		/// auto_increment
		/// </summary>        
		public int DIid { get; set; }
        /// <summary>
        /// DICode
        /// </summary>        
        public string DICode { get; set; }

        /// <summary>
        /// DIName
        /// </summary>        
        public string DIName { get; set; }
        /// <summary>
        /// DItype
        /// </summary>        
     
        public List<device_tree_model> Children { get; set; }

    }
}
