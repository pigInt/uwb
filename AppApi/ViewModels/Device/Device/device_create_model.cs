﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEntity.Entities;
namespace AppApi.ViewModels.Device.Device
{
    public class device_create_model
    {
        public int DIid { get; set; }

        /// <summary>
        /// DICode
        /// </summary>        
        public string DICode { get; set; }
        //public string remote_code { get; set; }
        //public string site_code { get; set; }
        /// <summary>
        /// DIName
        /// </summary>        
        public string DIName { get; set; }
        /// <summary>
        /// DItype
        /// </summary>        
        public string DItype { get; set; }

        public string parent_code { get; set; }
        //public int FarterID { get; set; }
        /// <summary>
        /// citycode
        /// </summary>        
        public string citycode { get; set; }
        /// <summary>
        /// zonecode
        /// </summary>        
        public string zonecode { get; set; }

        public int Status { get; set; }

        public int IsDeleted { get; set; }
    }
}
