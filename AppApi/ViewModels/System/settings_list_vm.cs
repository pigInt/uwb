﻿using AppEntity.Entities;
using System;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppApi.ViewModels.System
{
    /// <summary>
    /// 
    /// </summary>
    public class settings_list_vm
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }

        public Status Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IsDeleted IsDeleted { get; set; }
        public string CreatedOn { get; set; }
    }
}
