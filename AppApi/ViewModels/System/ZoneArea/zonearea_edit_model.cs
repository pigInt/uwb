﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.ViewModels.System.ZoneArea
{
    public class zonearea_edit_model
    {

        /// <summary>
        /// auto_increment
        /// </summary>        
        public int aid { get; set; }
        /// <summary>
        /// zonecode
        /// </summary>        
        public string zonecode { get; set; }
        /// <summary>
        /// zonename
        /// </summary>        
        public string zonename { get; set; }
        /// <summary>
        /// zid
        /// </summary>        
        public int zid { get; set; }
        /// <summary>
        /// zonekey
        /// </summary>        
        public string zonekey { get; set; }
        /// <summary>
        /// status
        /// </summary>        
        public int status { get; set; }
        /// <summary>
        /// desc
        /// </summary>        
        public string desc { get; set; }
    }
}
