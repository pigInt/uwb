﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppApi.ViewModels.System.City
{
    public class city_json_model
    {
        /// <summary>
        /// auto_increment
        /// </summary>        
        public int zid { get; set; }
        /// <summary>
        /// provinceCode
        /// </summary>        
        public string provinceCode { get; set; }
        /// <summary>
        /// citycode
        /// </summary>        
        public string citycode { get; set; }
        /// <summary>
        /// cityname
        /// </summary>        
        public string cityname { get; set; }
        /// <summary>
        /// status
        /// </summary>        
        public int status { get; set; }
        /// <summary>
        /// desc
        /// </summary>        
        public string desc { get; set; }
    }
}
