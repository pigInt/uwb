﻿using System;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppApi.ViewModels.Gateway
{
    public class device_vm
    {
        /// <summary>
        /// auto_increment
        /// </summary>        
        public int id { get; set; }
        /// <summary>
        /// code
        /// </summary>        
        public string code { get; set; }
        /// <summary>
        /// name
        /// </summary>        
        public string name { get; set; }
        /// <summary>
        /// type
        /// </summary>        
        public string type { get; set; }
        /// <summary>
        /// model
        /// </summary>        
        public string model { get; set; }
        /// <summary>
        /// mac_id
        /// </summary>        
        public string mac_id { get; set; }
        /// <summary>
        /// position_x
        /// </summary>        
        public int position_x { get; set; }
        /// <summary>
        /// position_y
        /// </summary>        
        public int position_y { get; set; }
        /// <summary>
        /// position_z
        /// </summary>        
        public int position_z { get; set; }

        /// <summary>
        /// region_guid
        /// </summary>        
        public int? region_guid { get; set; }

        /// <summary>
        /// parent_id
        /// </summary>        
        public int? parent_id { get; set; }

    }
}
