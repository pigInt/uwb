﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEntity.Entities;
namespace AppApi.ViewModels.Gateway
{
    public class attribute_model
    {
        public int id { get; set; }

        /// <summary>
        /// DICode
        /// </summary>        
        public string device_code { get; set; }

        public List<attributes> attributes { get; set; }


    }

    public class attributes
    {
        public string param_key { get; set; }

        public string param_value { get; set; }

    }
}
