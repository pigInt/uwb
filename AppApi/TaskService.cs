﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using AppRules;

namespace AppApi
{
    public class TaskService : BackgroundService
    {
        private readonly ILogger _logger;

        public TaskService(ILogger<TaskService> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation($"Consume Scoped Service Hosted Service running.{DateTime.Now}");
            //AppApi.Service.Mqtt.MqttServer.Start();
            //AppRules.SerialPortLib.open_serial("","");
            //AppRules.SerialPortLib.Listen();
            //AppRules.SerialPortLib.
            //new AppApi.Service.Mqtt.MqttClient(_logger).Start();
            //while (!stoppingToken.IsCancellationRequested)
            //{
            //    _logger.LogInformation($" Hosted Service is working. {DateTime.Now}");
            //    await Task.Delay(5000, stoppingToken);
            //}

        }
    }
}
