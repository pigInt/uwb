﻿using AppEntity.Entities;
using AppApi.Extensions;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AppApi.ViewModels.Upload;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AppCore;

namespace AppApi.Controllers.Api.V1
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/v1/[controller]/[action]")]
    [ApiController]

    public class UploadController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment environment;

        //public UploadController(IHostingEnvironment environment)
        //{
        //    this.environment = environment;
        //}
        public UploadController(DncZeusDbContext dbContext, IMapper mapper, IHostingEnvironment environment)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            this.environment = environment;
        }

        [HttpPost]
        public async Task<IActionResult> Img(IFormFile formFile)     //public async Task<IActionResult> UploadPic(IFormFile formFile)
        {
            var response = ResponseModelFactory.CreateInstance;
            //var allFiles = Request.Form.Files; // 多文件的话可以直接从 form 拿到完, 或则设置成 List<IFormFile> 就可以了
            var root = environment.WebRootPath;
            var extension = Path.GetExtension(formFile.FileName);
            DateTimeOffset dto = new DateTimeOffset(DateTime.Now);
            var guid = dto.ToUnixTimeSeconds().ToString();
            //var guid =  Guid.NewGuid().ToString();
            var fullPath = $@"{root}\upload\{guid + extension}";
            using (FileStream stream = new FileStream(fullPath, FileMode.Create))
            {
                await formFile.CopyToAsync(stream);
            }
            //upload_pic_res data = new upload_pic_res();
            //data.url = $@"\upload\{guid + extension}";
            //data.filename = guid + extension;
            response.SetData(guid + extension);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Files(IFormFileCollection files)
        {
            var response = ResponseModelFactory.CreateInstance;
            List<string> newfiles = new List<string>();
            upload_pic_res data = new upload_pic_res();
            string fileExt = ""; long fileSize = 0; string newFileName = "";
            var file = Request.Form.Files;
            long size = files.Count;
            string webRootPath = environment.WebRootPath;
            string contentRootPath = environment.ContentRootPath;
            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    fileExt = Path.GetExtension(formFile.FileName); //文件扩展名，不含“.”
                    fileSize = formFile.Length; //获得文件大小，以字节为单位
                    newFileName = Guid.NewGuid().ToString() + fileExt; //随机生成新的文件名
                    var filePath = webRootPath + "/upload/" + newFileName;
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {

                        await formFile.CopyToAsync(stream);
                        //data = new upload_pic_res();
                        //data.url = $@"\upload\{newFileName}";
                        //data.filename = newFileName;
                        newfiles.Add(newFileName);
                    }
                }
            }
            response.SetData(newfiles);
            return Ok(response);
        }
        [HttpGet("{filename}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> DeleteImg(string filename)
        {
            var response = ResponseModelFactory.CreateInstance;
            var root = environment.WebRootPath;
            var fullPath = $@"{root}\upload\{filename}";
            if (FileHelper.IsExistFile(fullPath))
            {
                FileHelper.DeleteFile(fullPath);
            }
            return Ok(response);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteFiles(List<string> files)
        {
            var response = ResponseModelFactory.CreateInstance;
            var root = environment.WebRootPath;
            var fullPath = "";
            foreach (var filename in files)
            {
                fullPath = $@"{root}\upload\{filename}";

                if (FileHelper.IsExistFile(fullPath))
                {
                    FileHelper.DeleteFile(fullPath);
                }
            }
            return Ok(response);
        }

    }
    public class upload_m
    {
        public IFormFile upload_model { set; get; }
    }
}
