﻿using AutoMapper;
using System.Linq;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload;
using AppApi.ViewModels.System.City;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;
namespace AppApi.Controllers.Api.V1.System
{
    [Route("api/v1/system/[controller]/[action]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public CityController(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        /// <summary>
        /// 配置列表
        /// </summary>
        /// <param name="payload">请求参数</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult List(AppApi.RequestPayload.RequestPayload payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.dnc_city.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.citycode.Contains(payload.Kw.Trim()) || x.cityname.Contains(payload.Kw.Trim()));
                }

                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "DESC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).ToList();
                var totalCount = query.Count();
                var data = list.Select(_mapper.Map<dnc_city, city_json_model>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/system/city/select")]
        public IActionResult Select()
        {
            var response = ResponseModelFactory.CreateResultInstance;

            using (_dbContext)
            {
                var query = _dbContext.dnc_city.AsQueryable();
                var list = query.ToList();
                var data = list.Select(x => new { x.zid, x.citycode, x.cityname });

                response.SetData(data);
                return Ok(response);
            }
        }
        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Create(city_add_model model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (model.citycode.Trim().Length <= 0)
            {
                response.SetFailed("请输入编号");
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.dnc_city.Count(x => x.citycode == model.citycode) > 0)
                {
                    response.SetFailed("编号已存在");
                    return Ok(response);
                }
                var entity = _mapper.Map<city_add_model, dnc_city>(model);
                entity.provincecode = model.provincecode;
                entity.citycode = model.citycode;
                entity.cityname = model.cityname;
                entity.status = model.status;
                entity.desc = model.desc;
                _dbContext.dnc_city.Add(entity);
                _dbContext.SaveChanges();

                response.SetSuccess();
                return Ok(response);
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(int id)
        {
            using (_dbContext)
            {
                var entity = _dbContext.dnc_city.FirstOrDefault(x => x.zid == id);
                var response = ResponseModelFactory.CreateInstance;
                response.SetData(_mapper.Map<dnc_city, city_edit_model>(entity));
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Edit(city_edit_model model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.dnc_city.Count(x => x.citycode == model.citycode && x.zid != model.zid) > 0)
                {
                    response.SetFailed("编号名称已存在");
                    return Ok(response);
                }
                var entity = _dbContext.dnc_city.FirstOrDefault(x => x.zid == model.zid);
                entity.provincecode = model.provincecode;
                entity.citycode = model.citycode;
                entity.cityname = model.cityname;
                entity.status = model.status;
                entity.desc = model.desc;
                _dbContext.SaveChanges();
                response = ResponseModelFactory.CreateInstance;
                return Ok(response);
            }
        }
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Delete(string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            response = UpdateIsDelete(1, ids);
            return Ok(response);
        }
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Recover(string ids)
        {
            var response = UpdateIsDelete(0, ids);
            return Ok(response);
        }
        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Batch(string command, string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            switch (command)
            {
                case "delete":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateIsDelete(1, ids);
                    break;
                case "recover":
                    response = UpdateIsDelete(0, ids);
                    break;
                case "forbidden":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateStatus(1, ids);
                    break;
                case "normal":
                    response = UpdateStatus(0, ids);
                    break;
                default:
                    break;
            }
            return Ok(response);
        }
        #region 私有方法
        /// <summary>
        /// 删除配置
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private ResponseModel UpdateIsDelete(int isDeleted, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((zid, index) => new MySqlParameter(string.Format("@p{0}", index), zid)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                //var sql = string.Format("UPDATE dnc_city SET IsDeleted=@IsDeleted WHERE Id IN ({0})", parameterNames);
                //parameters.Add(new MySqlParameter("@IsDeleted", (int)isDeleted));
                var sql = string.Format("delete from dnc_city WHERE zid IN ({0})", parameterNames);
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }
        /// <summary>
        /// 删除配置
        /// </summary>
        /// <param name="status">角色状态</param>
        /// <param name="ids">角色ID字符串,多个以逗号隔开</param>
        /// <returns></returns>
        private ResponseModel UpdateStatus(int status, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("UPDATE dnc_city SET Status=@Status WHERE Id IN ({0})", parameterNames);
                parameters.Add(new MySqlParameter("@Status", (int)status));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }
        #endregion
    }
}
    

