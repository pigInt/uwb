﻿using AutoMapper;
using System.Linq;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload;
using AppApi.ViewModels.System.ZoneArea;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;

namespace AppApi.Controllers.Api.V1.System
{
    [Route("api/v1/system/[controller]/[action]")]
    [ApiController]
    public class ZoneAreaController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public ZoneAreaController(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        /// <summary>
        /// 配置列表
        /// </summary>
        /// <param name="payload">请求参数</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult List(RequestPayload.RequestPayload payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.dnc_zonearea.AsQueryable();
                //var sql = @"select aid, zone.zid,zonecode,zonename,city.citycode,city.cityname from dnc_zonearea zone 
                //        left outer join(select zid,citycode,cityname from dnc_city)city on zone.zid=city.zid";
                //var query = _dbContext.dnc_zonearea.FromSql(sql).ToList();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.zonecode.Contains(payload.Kw.Trim()) || x.zonename.Contains(payload.Kw.Trim()));
                }

                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "DESC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).ToList();
                var totalCount = query.Count();
                var data = list.Select(_mapper.Map<dnc_zonearea, zonearea_json_model>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/system/zonearea/select/{id}")]
        public IActionResult FindByKeyword(int id)
        {
            var response = ResponseModelFactory.CreateResultInstance;
            if (id <=0)
            {
                response.SetFailed("没有查询到数据");
                return Ok(response);
            }
            using (_dbContext)
            {
                var query = _dbContext.dnc_zonearea.AsQueryable();
                query = query.Where(x => x.zid==id);

                var list = query.ToList();
                var data = list.Select(x => new {x.zonecode, x.zonename });

                response.SetData(data);
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Create(zonearea_add_model model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (model.zonecode.Trim().Length <= 0)
            {
                response.SetFailed("请输入编号");
                return Ok(response);
            }
            if (model.zonename.Trim().Length <= 0)
            {
                response.SetFailed("请输入名称");
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.dnc_zonearea.Count(x => x.zonecode == model.zonecode) > 0)
                {
                    response.SetFailed("编号已存在");
                    return Ok(response);
                }
                var entity = _mapper.Map<zonearea_add_model, dnc_zonearea>(model);
                entity.zonecode = model.zonecode;
                entity.zonename = model.zonename;
                entity.zid = model.zid;
                entity.zonekey = model.zonekey;
                entity.status = model.status;
                entity.desc = model.desc;
                _dbContext.dnc_zonearea.Add(entity);
                _dbContext.SaveChanges();

                response.SetSuccess();
                return Ok(response);
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(int id)
        {
            using (_dbContext)
            {
                var entity = _dbContext.dnc_zonearea.FirstOrDefault(x => x.aid == id);
                var response = ResponseModelFactory.CreateInstance;
                response.SetData(_mapper.Map<dnc_zonearea, zonearea_edit_model>(entity));
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Edit(zonearea_edit_model model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.dnc_zonearea.Count(x => x.zonecode == model.zonecode && x.aid != model.aid) > 0)
                {
                    response.SetFailed("编号已存在");
                    return Ok(response);
                }
                var entity = _dbContext.dnc_zonearea.FirstOrDefault(x => x.zid == model.zid);
                entity.zonecode = model.zonecode;
                entity.zonename = model.zonename;
                entity.zid = model.zid;
                entity.zonekey = model.zonekey;
                entity.status = model.status;
                entity.desc = model.desc;
                _dbContext.SaveChanges();
                response = ResponseModelFactory.CreateInstance;
                return Ok(response);
            }
        }
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Delete(string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            response = UpdateIsDelete(CommonEnum.IsDeleted.Yes, ids);
            return Ok(response);
        }
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Recover(string ids)
        {
            var response = UpdateIsDelete(CommonEnum.IsDeleted.No, ids);
            return Ok(response);
        }
        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Batch(string command, string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            switch (command)
            {
                case "delete":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateIsDelete(CommonEnum.IsDeleted.Yes, ids);
                    break;
                case "recover":
                    response = UpdateIsDelete(CommonEnum.IsDeleted.No, ids);
                    break;
                case "forbidden":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateStatus(UserStatus.Forbidden, ids);
                    break;
                case "normal":
                    response = UpdateStatus(UserStatus.Normal, ids);
                    break;
                default:
                    break;
            }
            return Ok(response);
        }
        #region 私有方法
        /// <summary>
        /// 删除配置
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        private ResponseModel UpdateIsDelete(CommonEnum.IsDeleted isDeleted, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((aid, index) => new MySqlParameter(string.Format("@p{0}", index), aid)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                //var sql = string.Format("UPDATE dnc_zonearea SET IsDeleted=@IsDeleted WHERE aid IN ({0})", parameterNames);
                var sql = string.Format("delete from  dnc_zonearea WHERE aid IN ({0})", parameterNames);
                //parameters.Add(new MySqlParameter("@IsDeleted", (int)isDeleted));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }
        /// <summary>
        /// 删除配置
        /// </summary>
        /// <param name="status">角色状态</param>
        /// <param name="ids">角色ID字符串,多个以逗号隔开</param>
        /// <returns></returns>
        private ResponseModel UpdateStatus(UserStatus status, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("UPDATE dnc_zonearea SET Status=@Status WHERE Id IN ({0})", parameterNames);
                parameters.Add(new MySqlParameter("@Status", (int)status));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }
        #endregion
    }
}

