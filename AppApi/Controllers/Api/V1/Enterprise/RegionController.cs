﻿using AutoMapper;
using System.Linq;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload.Enterprise;
using AppApi.ViewModels.Enterprise;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;
using System.Collections.Generic;

namespace AppApi.Controllers.Api.V1.Enterprise
{
    [Route("api/v1/enterprise/[controller]/[action]")]
    [ApiController]
    public class RegionController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public RegionController(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        #region  Method
        [HttpPost]
        public IActionResult List(region_rq payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.region.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.name.Contains(payload.Kw.Trim()));
                }
              

                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "DESC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).ToList();
                var totalCount = query.Count();
                var data = list.Select(_mapper.Map<region, region_list_vm>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Create(region_create_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;

            if (model.name == "")
            {
                response.SetFailed("请输入区域名称");
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.region.Count(x => x.name == model.name) > 0)
                {
                    response.SetFailed("区域名称已存在");
                    return Ok(response);
                }

                //var entity = _mapper.Map<region_create_vm, region>(model);
                var entity = new region();
                entity.parent_guid = model.parent_guid;
                entity.parent_name = model.parent_name;
                entity.name = model.name;
                entity.position = model.position;
                entity.length = model.length;
                entity.width = model.width;
                entity.height = model.height;
                entity.background = model.background;
                entity.background_ratio = model.background_ratio;
                entity.longitude = model.longitude;
                entity.latitude = model.latitude;
                entity.create_time = DateTime.Now;
                entity.modify_time = DateTime.Now;
                _dbContext.region.Add(entity);
                _dbContext.SaveChanges();

                response.SetSuccess();
                return Ok(response);
            }
        }

        [HttpGet("{guid}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(int guid)
        {
            using (_dbContext)
            {
                var entity = _dbContext.region.FirstOrDefault(x => x.guid == guid);
                var response = ResponseModelFactory.CreateInstance;
                var model = _mapper.Map<region, region_edit_vm>(entity);
                //response.SetData(_mapper.Map<department, department_edit_vm>(entity));
                var tree = LoadRegionTree(model.parent_guid.ToString());
                response.SetData(new { model, tree });
                return Ok(response);
            }
        }
        [HttpGet("{guid}")]
        [ProducesResponseType(200)]
        public IActionResult Select(int guid)
        {
            using (_dbContext)
            {
                var entity = _dbContext.region.FirstOrDefault(x => x.guid == guid);
                var response = ResponseModelFactory.CreateInstance;
                var model = _mapper.Map<region, region_edit_vm>(entity);
                //response.SetData(_mapper.Map<department, department_edit_vm>(entity));
                response.SetData(model);
                return Ok(response);
            }
        }
        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Edit(region_edit_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.region.Count(x => x.name == model.name && x.guid != model.guid) > 0)
                {
                    response.SetFailed("名称已存在");
                    return Ok(response);
                }
                var entity = _dbContext.region.FirstOrDefault(x => x.guid == model.guid);
                entity.parent_guid = model.parent_guid;
                entity.parent_name = model.parent_name;
                entity.create_time = DateTime.Now;
                entity.modify_time = DateTime.Now;
                entity.name = model.name;
                entity.position = model.position;
                entity.length = model.length;
                entity.width = model.width;
                entity.height = model.height;
                entity.background = model.background;
                entity.background_ratio = model.background_ratio;
                entity.longitude = model.longitude;
                entity.latitude = model.latitude;
                _dbContext.SaveChanges();
                response = ResponseModelFactory.CreateInstance;
                return Ok(response);
            }
        }

        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Delete(string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            response = UpdateIsDelete(1, ids);
            return Ok(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Recover(string ids)
        {
            var response = UpdateIsDelete(0, ids);
            return Ok(response);
        }

        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Batch(string command, string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            switch (command)
            {
                case "delete":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateIsDelete(1, ids);
                    break;
                case "recover":
                    response = UpdateIsDelete(0, ids);
                    break;
                case "forbidden":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateStatus(1, ids);
                    break;
                case "normal":
                    response = UpdateStatus(0, ids);
                    break;
                default:
                    break;
            }
            return Ok(response);
        }
        /// <summary>
        /// 菜单树
        /// </summary>
        /// <returns></returns>
        [HttpGet("{selectid?}")]
        public IActionResult Tree(string selectid)
        {
            var response = ResponseModelFactory.CreateInstance;
            var tree = LoadRegionTree(selectid);
            response.SetData(tree);
            return Ok(response);
        }

        /// <summary>
        /// 菜单树
        /// </summary>
        /// <returns></returns>
        [HttpGet("{selectid?}")]
        public IActionResult TreeList(string selectid)
        {
            var response = ResponseModelFactory.CreateInstance;
            var temp = _dbContext.region.ToList().Select(x => new region_tree_vm
            {
                Guid = x.guid.ToString(),
                ParentGuid = x.parent_guid.ToString(),
                Title = x.name
            }).ToList();
            var root = new region_tree_vm
            {
                Title = "区域管理",
                Guid = "0",// Guid.Empty.ToString(),
                ParentGuid = null
            };
            temp.Insert(0, root);
            var tree = temp.BuildTree(selectid);
            response.SetData(tree);
            return Ok(response);
        }

        #endregion

        #region 私有方法
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <param name="ids">用户ID字符串,多个以逗号隔开</param>
        /// <returns></returns>
        private ResponseModel UpdateIsDelete(int isDeleted, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("delete from region WHERE guid IN ({0})", parameterNames);
                //var sql = string.Format("UPDATE label SET IsDeleted=@IsDeleted WHERE DIid IN ({0})", parameterNames);
                //parameters.Add(new MySqlParameter("@IsDeleted", (int)isDeleted));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }

        private ResponseModel UpdateStatus(int status, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("UPDATE region SET status=@Status WHERE guid IN ({0})", parameterNames);
                parameters.Add(new MySqlParameter("@Status", (int)status));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }


        private List<region_tree_vm> LoadRegionTree(string selectedid = null)
        {
            var temp = _dbContext.region.ToList().Select(x => new region_tree_vm
            {
                Guid = x.guid.ToString(),
                ParentGuid = x.parent_guid.ToString(),
                Title = x.name
            }).ToList();
            var root = new region_tree_vm
            {
                Title = "顶级名称",
                Guid = "0",// Guid.Empty.ToString(),
                ParentGuid = null
            };
            temp.Insert(0, root);
            var tree = temp.BuildTree(selectedid);
            return tree;
        }

        #endregion
    }

    public static class RegionTreeHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="selectedid"></param>
        /// <returns></returns>
        public static List<region_tree_vm> BuildTree(this List<region_tree_vm> menus, string selectedid = null)
        {
            var lookup = menus.ToLookup(x => x.ParentGuid);
            Func<string, List<region_tree_vm>> build = null;
            build = pid =>
            {
                return lookup[pid]
                 .Select(x => new region_tree_vm()
                 {
                     Guid = x.Guid,
                     ParentGuid = x.ParentGuid,
                     Title = x.Title,
                     Expand = (x.ParentGuid == null || x.ParentGuid == "0"),//Guid.Empty
                     Selected = selectedid == x.Guid,
                     Children = build(x.Guid),//new Guid(x.Guid)
                 })
                 .ToList();
            };
            var result = build(null);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regions"></param>
        /// <param name="ParentGuid"></param>
        /// <returns></returns>
        public static List<region_tree_vm> fill_region(List<region> regions, int? ParentGuid, string selectedid = null)
        {
            List<region_tree_vm> recursiveObjects = new List<region_tree_vm>();
            foreach (var item in regions.Where(x => x.guid == ParentGuid))
            {
                var children = new region_tree_vm
                {
                    Expand = true,
                    Guid = item.guid.ToString(),
                    Title = item.name,
                    Children = fill_region(regions, item.parent_guid)
                };
                recursiveObjects.Add(children);
            }
            return recursiveObjects;
        }

    }

}
