﻿using AutoMapper;
using System.Linq;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload.Enterprise;
using AppApi.ViewModels.Enterprise;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;

namespace AppApi.Controllers.Api.V1.Enterprise
{
    [Route("api/v1/enterprise/[controller]/[action]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public EmployeeController(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        #region  Method
        [HttpPost]
        public IActionResult List(employee_rq payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.employee.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.name.Contains(payload.Kw.Trim()));
                }
                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.status == payload.Status);
                }
                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "DESC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).ToList();
                var totalCount = query.Count();
                var data = list.Select(_mapper.Map<employee, employee_list_vm>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Create(employee_create_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;

            if (model.code =="")
            {
                response.SetFailed("请输入员工编号");
                return Ok(response);
            }
            if (model.name == "")
            {
                response.SetFailed("请输入员工姓名");
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.device_tag.Count(x => x.code == model.code) > 0)
                {
                    response.SetFailed("员工编号已存在");
                    return Ok(response);
                }

                //var entity = _mapper.Map<employee_create_vm, employee>(model);
                var entity = new employee();
                entity.guid= Guid.NewGuid();
                entity.code = model.code;
                entity.name = model.name;
                entity.sex = model.sex;
                entity.age = model.age;
                entity.position = model.position;
                entity.department = model.department;
                entity.telephone = model.telephone;
                entity.status = model.status;
                entity.create_time = DateTime.Now;
                entity.modify_time = DateTime.Now;
                _dbContext.employee.Add(entity);
                _dbContext.SaveChanges();

                response.SetSuccess();
                return Ok(response);
            }
        }

        [HttpGet("{guid}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(Guid guid)
        {
            using (_dbContext)
            {
                var entity = _dbContext.employee.FirstOrDefault(x => x.guid == guid);
                var response = ResponseModelFactory.CreateInstance;
                response.SetData(_mapper.Map<employee, employee_edit_vm>(entity));
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Edit(employee_edit_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.employee.Count(x => x.code == model.code && x.guid != model.guid) > 0)
                {
                    response.SetFailed("员工编号已存在");
                    return Ok(response);
                }
                var entity = _dbContext.employee.FirstOrDefault(x => x.guid == model.guid);
                entity.code = model.code;
                entity.name = model.name;
                entity.sex = model.sex;
                entity.age = model.age;
                entity.position = model.position;
                entity.department = model.department;
                entity.telephone = model.telephone;
                entity.status = model.status;
                entity.create_time = DateTime.Now;
                entity.modify_time = DateTime.Now;
                _dbContext.SaveChanges();
                response = ResponseModelFactory.CreateInstance;
                return Ok(response);
            }
        }

        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Delete(string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            response = UpdateIsDelete(1, ids);
            return Ok(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Recover(string ids)
        {
            var response = UpdateIsDelete(0, ids);
            return Ok(response);
        }

        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Batch(string command, string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            switch (command)
            {
                case "delete":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateIsDelete(1, ids);
                    break;
                case "recover":
                    response = UpdateIsDelete(0, ids);
                    break;
                case "forbidden":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateStatus(0, ids);
                    break;
                case "normal":
                    response = UpdateStatus(1, ids);
                    break;
                default:
                    break;
            }
            return Ok(response);
        }

        #endregion

        #region 私有方法
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <param name="ids">用户ID字符串,多个以逗号隔开</param>
        /// <returns></returns>
        private ResponseModel UpdateIsDelete(int isDeleted, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("delete from employee WHERE guid IN ({0})", parameterNames);
                //var sql = string.Format("UPDATE label SET IsDeleted=@IsDeleted WHERE DIid IN ({0})", parameterNames);
                //parameters.Add(new MySqlParameter("@IsDeleted", (int)isDeleted));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }

        private ResponseModel UpdateStatus(int status, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("UPDATE employee SET status=@Status WHERE guid IN ({0})", parameterNames);
                parameters.Add(new MySqlParameter("@Status", (int)status));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }
        #endregion
    }
}
