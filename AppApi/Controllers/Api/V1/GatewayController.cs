﻿using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload.Gateway;
using AppApi.ViewModels.Gateway;
using AppApi.ViewModels.Device;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;
using AppEntity.Entities.QueryModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.Logging;

namespace AppApi.Controllers.Api.V1.Gateway
{
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class GatewayController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        //public GatewayController()
        //{

        //}
        public GatewayController(DncZeusDbContext dbContext, IMapper mapper, ILogger<GatewayController> logger)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
        }
        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Attribute(string  request_json)
        {
            var response = ResponseModelFactory.CreateInstance;
            var request = JsonConvert.DeserializeObject<attribute_model>(request_json);

            //response = new Extensions.Gateway().attribute_edit(request);

            return Ok(response);

        }
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult get_device()
        {
            using (_dbContext)
            {
                var query = _dbContext.device.AsQueryable();
                query = query.Where(x => x.status == CommonEnum.Status.Normal);
                query = query.OrderBy("id", true);
                var data = query.ToList().Select(_mapper.Map<device, device_vm>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data);
                return Ok(response);
            }
        }
        [HttpPost]
        //[ProducesResponseType(200)]
        public IActionResult set_tag(request_tag_rp request)//List<device_tag_rp> list_tag
        {
            string tmpstr = "";
            var response = ResponseModelFactory.CreateInstance;
            if (request == null)
            {
                response.SetFailed("Request Json is Null");
                return Ok(response);
            }

            using (_dbContext)
            {
                try
                {
                    //_logger.LogInformation("请求："+request_json);
                    device_list_vm m_device = new device_list_vm();
                    foreach (device_tag_rp model in request.request_data)
                    {
                        var ent_device = _dbContext.device.FirstOrDefault(x => x.mac_id == model.device_id);
                        //m_device = _mapper.Map<device, device_list_vm>(ent_device);
                        //Mapper.Initialize(x => x.CreateMap<device, device_list_vm>());
                        m_device = new device_list_vm()
                        {
                            id = ent_device.id,
                            code = ent_device.code,
                            mac_id = ent_device.mac_id,
                            position_left = ent_device.position_left,
                            position_top = ent_device.position_top,
                            region_guid = (int)ent_device.region_guid
                        };
                        //m_device = Mapper.Map<device_list_vm>(ent_device);

                        if (m_device == null)
                        {
                            continue;
                        }
                        if (_dbContext.tag_extend.Count(x => x.unique_id == model.unique_id) == 0) //新增记录
                        {
                            //var entity = _mapper.Map<device_tag_rp, tag_extend>(model);
                            var entity = new tag_extend();
                            entity.unique_id = model.unique_id;
                            entity.position_left = m_device.position_left;
                            entity.position_top = m_device.position_top;
                            entity.position_x = model.position_x;
                            entity.position_y = model.position_y;
                            entity.position_x = model.position_z;
                            entity.region_guid = m_device.region_guid;
                            entity.create_time = Convert.ToDateTime(model.create_time);
                            entity.modify_time = DateTime.Now;
                            _dbContext.tag_extend.Add(entity);
                            _dbContext.SaveChanges();
                        }
                        else
                        {
                            //保存标签扩展表
                            var entity = _dbContext.tag_extend.FirstOrDefault(x => x.unique_id == model.unique_id);
                            entity.position_left = m_device.position_left;
                            entity.position_top = m_device.position_top;
                            entity.position_x = model.position_x;
                            entity.position_y = model.position_y;
                            entity.position_z = model.position_z;
                            entity.region_guid = m_device.region_guid;
                            entity.modify_time = DateTime.Now;
                            _dbContext.SaveChanges();
                        }
                        //保存标签轨迹表
                        //var entity1 = _mapper.Map<device_tag_rp, tag_trajectory>(model);
                        var entity1 = new tag_trajectory();
                        entity1.unique_id = model.unique_id;
                        entity1.device_id = ent_device.id;
                        entity1.position_left = m_device.position_left;
                        entity1.position_top = m_device.position_top;
                        entity1.position_x = model.position_x;
                        entity1.position_y = model.position_y;
                        entity1.position_z = model.position_z;
                        entity1.region_guid = m_device.region_guid;
                        entity1.create_time = Convert.ToDateTime(model.create_time);
                        entity1.create_millisecond = model.create_millisecond;
                        _dbContext.tag_trajectory.Add(entity1);
                        _dbContext.SaveChanges();

                    }
                }
                catch(Exception ex)
                {
                    response.SetFailed("数据异常："+ex.Message);
                }
                return Ok(response);
            }
        }
    }

    public class GatewayHelp
    {

    }
}

