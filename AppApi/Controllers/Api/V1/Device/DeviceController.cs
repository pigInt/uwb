﻿using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload.Device;
using AppApi.ViewModels.Device;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;
using AppEntity.Entities.QueryModels;
using AppApi.Controllers.Api.V1.Enterprise;
using AppApi.ViewModels.Enterprise;

namespace AppApi.Controllers.Api.V1.Device
{
    [Route("api/v1/Device/[controller]/[action]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public DeviceController(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        [HttpPost]
        public IActionResult List(DeviceRequestPayload payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.device.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.name.Contains(payload.Kw.Trim()) || x.code.Contains(payload.Kw.Trim()));
                }
                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.status == payload.Status);
                }

                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "DESC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).ToList();
                var totalCount = query.Count();
                var data = list.Select(_mapper.Map<device, device_create_vm>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }
        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Create(device_create_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (model.code.Trim().Length <= 0)
            {
                response.SetFailed("请输入编号");
                return Ok(response);
            }
            if (model.name.Trim().Length <= 0)
            {
                response.SetFailed("请输入名称");
                return Ok(response);
            }
            if (model.region_guid <= 0)
            {
                response.SetFailed("请选择区域");
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.device.Count(x => x.code == model.code) > 0)
                {
                    response.SetFailed("设备已存在");
                    return Ok(response);
                }

                //var entity = _mapper.Map<device_create_vm, device>(model);
                var entity = new device();
                entity.status = model.status;
                entity.create_time = DateTime.Now;
                entity.modify_time = DateTime.Now;
                entity.region_guid = model.region_guid;
                entity.parent_id = model.parent_id;
                entity.code = model.code;
                entity.name = model.name;
                entity.type = model.type;
                entity.model = model.model;
                entity.mac_id = model.mac_id;
                entity.position_left = model.position_left;
                entity.position_top = model.position_top;
                entity.position_x = model.position_x;
                entity.position_y = model.position_y;
                entity.position_z = model.position_z;
                entity.sort_num = model.sort_num;
                _dbContext.device.Add(entity);
                _dbContext.SaveChanges();
                response.SetSuccess();
                return Ok(response);
            }
        }
        [HttpGet("{guid}")]
        [ProducesResponseType(200)]
        public IActionResult Region(int guid)
        {
            using (_dbContext)
            {
                var entity1 = _dbContext.region.FirstOrDefault(x => x.guid == guid);
                var regioninfo = _mapper.Map<region, region_edit_vm>(entity1);

                var deviceinfo = new List<device_edit_vm>();
                device_edit_vm model = null;
                var query = _dbContext.device.AsQueryable();
                var list = query.Where(x => x.region_guid == guid).ToList();
                foreach (device entity in list)
                {
                    model = new device_edit_vm();
                    model.id = entity.id;
                    model.status = entity.status;
                    model.modify_time = entity.modify_time.ToString("yyyy-MM-dd hh:mi:ss");
                    model.region_guid = entity.region_guid;
                    if (model.region_guid != null)
                        model.region_name = _dbContext.region.FirstOrDefault(x => x.guid == model.region_guid).name;
                    else model.region_name = "";
                    model.parent_id = (int)entity.parent_id;
                    if (model.parent_id > 0)
                        model.parent_name = _dbContext.device.FirstOrDefault(x => x.id == model.parent_id).name;
                    model.code = entity.code;
                    model.name = entity.name;
                    model.type = entity.type;
                    model.model = entity.model;
                    model.mac_id = entity.mac_id;
                    model.position_left = entity.position_left;
                    model.position_top = entity.position_top;
                    model.position_x = entity.position_x;
                    model.position_y = entity.position_y;
                    model.position_z = entity.position_z;
                    model.sort_num = entity.sort_num;
                    deviceinfo.Add(model);
                }

                var response = ResponseModelFactory.CreateInstance;
                //var model = _mapper.Map<device, device_edit_vm>(entity);
                response.SetData(new { regioninfo, deviceinfo });
                return Ok(response);
            }
        }
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(int id)
        {
            using (_dbContext)
            {
                var entity = _dbContext.device.FirstOrDefault(x => x.id == id);
                //var entity = _dbContext.device.FromSql("SELECT device.*,b.name as region_name FROM device left outer join(select guid,name from region)b on device.region_guid=b.guid").Where(x=>x.id==id);
                var response = ResponseModelFactory.CreateInstance;

                //var model = _mapper.Map<device, device_edit_vm>(entity);
                var model = new device_edit_vm();
                model.id = entity.id;
                model.status = entity.status;
                model.modify_time = entity.modify_time.ToString("yyyy-MM-dd hh:mi:ss");
                model.region_guid = entity.region_guid;
                if (model.region_guid != null && model.region_guid>0)
                    model.region_name = _dbContext.region.FirstOrDefault(x => x.guid == model.region_guid).name;
                else model.region_name = "";
                model.parent_id = (int)entity.parent_id;
                if (model.parent_id != null && model.parent_id>0)
                    model.parent_name = _dbContext.device.FirstOrDefault(x => x.id == model.parent_id).name;
                model.code = entity.code;
                model.name = entity.name;
                model.type = entity.type;
                model.model = entity.model;
                model.mac_id = entity.mac_id;
                model.position_left = entity.position_left;
                model.position_top = entity.position_top;
                model.position_x = entity.position_x;
                model.position_y = entity.position_y;
                model.position_z = entity.position_z;
                model.sort_num = entity.sort_num;
                var tree = LoadRegionTree(model.region_guid.ToString());
                response.SetData(new { model, tree });
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Edit(device_edit_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.device.Count(x => x.code == model.code && x.id != model.id) > 0)
                {
                    response.SetFailed("设备名称已存在");
                    return Ok(response);
                }
                var entity = _dbContext.device.FirstOrDefault(x => x.id == model.id);
                entity.status = model.status;
                entity.modify_time = DateTime.Now;
                entity.code = model.code;
                entity.name = model.name;
                entity.type = model.type;
                entity.model = model.model;
                entity.mac_id = model.mac_id;
                entity.position_left = model.position_left;
                entity.position_top = model.position_top;
                entity.position_x = model.position_x;
                entity.position_y = model.position_y;
                entity.position_z = model.position_z;
                entity.region_guid = model.region_guid;
                entity.parent_id = model.parent_id;
                entity.sort_num = model.sort_num;
                _dbContext.SaveChanges();
                response = ResponseModelFactory.CreateInstance;
                return Ok(response);
            }
        }
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Delete(string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            response = UpdateIsDelete(1, ids);
            return Ok(response);
        }
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Recover(string ids)
        {
            var response = UpdateIsDelete(0, ids);
            return Ok(response);
        }
        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Batch(string command, string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            switch (command)
            {
                case "delete":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateIsDelete(1, ids);
                    break;
                case "recover":
                    response = UpdateIsDelete(0, ids);
                    break;
                case "forbidden":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateStatus(0, ids);
                    break;
                case "normal":
                    response = UpdateStatus(1, ids);
                    break;
                default:
                    break;
            }
            return Ok(response);
        }
        /// <summary>
        /// 设备列表树
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/device/device/parent")]
        public IActionResult find_parent(device_tree_rq payload)
        {
            var response = ResponseModelFactory.CreateInstance;
            using (_dbContext)
            {
                var query = _dbContext.device.AsQueryable().Where(x => x.parent_id == 0);
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.name.Contains(payload.Kw.Trim()) || x.code.Contains(payload.Kw.Trim()));
                }

                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.status == payload.Status);
                }
                if (payload.parent_id > 0)
                {
                    query = query.Where(x => x.parent_id == payload.parent_id);

                }
                var list = query.ToList();
                var data = list.Select(x => new { x.id, x.code, x.name });
                response.SetData(data);
            }

            return Ok(response);
        }
        [HttpGet("/api/v1/device/device/parent_tree")]
        public IActionResult parent_tree()
        {
            var response = ResponseModelFactory.CreateInstance;
            using (_dbContext)
            {
                string sql = @"select * from device ";
                var devices = _dbContext.device.FromSql(sql).ToList();
                var tree = DeviceTreeHelper.fill_parent_tree(devices);
                response.SetData(tree);
            }

            return Ok(response);
        }

        #region 私有方法
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <param name="ids">用户ID字符串,多个以逗号隔开</param>
        /// <returns></returns>
        private ResponseModel UpdateIsDelete(int isDeleted, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("delete from device WHERE id IN ({0})", parameterNames);
                //parameters.Add(new MySqlParameter("@IsDeleted", (int)isDeleted));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }

        private ResponseModel UpdateStatus(int status, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("UPDATE device SET Status=@Status WHERE id IN ({0})", parameterNames);
                parameters.Add(new MySqlParameter("@Status", (int)status));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }

        private List<ViewModels.Enterprise.region_tree_vm> LoadRegionTree(string selectedid = null)
        {
            var temp = _dbContext.region.ToList().Select(x => new ViewModels.Enterprise.region_tree_vm
            {
                Guid = x.guid.ToString(),
                ParentGuid = x.parent_guid.ToString(),
                Title = x.name
            }).ToList();
            var root = new ViewModels.Enterprise.region_tree_vm
            {
                Title = "顶级区域",
                Guid = Guid.Empty.ToString(),
                ParentGuid = ""
            };
            temp.Insert(0, root);
            var tree = temp.BuildTree(selectedid);
            return tree;
        }
        #endregion
    }
    public static class DeviceTreeHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="devices"></param>
        /// <param name="parent_id"></param>
        /// <returns></returns>
        public static List<device_parent_tree_vm> fill_parent_tree(List<device> devices, int parent_id = 0)
        {
            parent_id = parent_id == null ? 0 : parent_id;
            List<device_parent_tree_vm> recursiveObjects = new List<device_parent_tree_vm>();
            foreach (var item in devices.Where(x => x.parent_id == parent_id))
            {
                var children = new device_parent_tree_vm
                {
                    Expand = true,
                    Code = item.id.ToString(),
                    Title = item.name,
                    ParentId = item.parent_id.ToString(),
                    Children = fill_parent_tree(devices, (int)item.id)
                };
                recursiveObjects.Add(children);
            }
            return recursiveObjects;
        }
    }
}

