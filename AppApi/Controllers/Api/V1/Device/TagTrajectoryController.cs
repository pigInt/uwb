﻿using AutoMapper;
using System.Linq;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload;
using AppApi.RequestPayload.Device;
using AppApi.ViewModels.Device;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;
using AppApi.RequestPayload.Gateway;
using System.Collections.Generic;
using AppApi.ViewModels.Enterprise;
using AppApi.Controllers.Api.V1.Enterprise;

namespace AppApi.Controllers.Api.V1.Device
{
    [Route("api/v1/device/[controller]/[action]")]
    [ApiController]
    public class TagTrajectoryController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public TagTrajectoryController(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        #region  Method
        [HttpPost]
        public IActionResult List(device_tag_rq payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.v_tag_trajectory.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.unique_id.Contains(payload.Kw.Trim()) || x.tag_name.Contains(payload.Kw.Trim()) || x.region_name.Contains(payload.Kw.Trim()));
                }
                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.status == payload.Status);
                }

                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "ASC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).ToList();
                var totalCount = query.Count();
                var data = list.Select(_mapper.Map<v_tag_trajectory, v_tag_trajectory_list_vm>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(int id)
        {
            using (_dbContext)
            {
                var entity = _dbContext.v_tag_trajectory.FirstOrDefault(x => x.id == id);
                var response = ResponseModelFactory.CreateInstance;
                response.SetData(_mapper.Map<v_tag_trajectory, v_tag_trajectory_list_vm>(entity));
                return Ok(response);
            }
        }

        [HttpPost]
        public IActionResult sel_trajectory(tag_trajectory_rp payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.v_tag_trajectory.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.unique_id.Contains(payload.Kw.Trim()) || x.tag_name.Contains(payload.Kw.Trim()) || x.tag_code.Contains(payload.Kw.Trim()));
                }
                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.status == payload.Status);
                }

                var tag_data = query.GroupBy(c => new { c.unique_id, c.tag_name, c.region_guid, c.device_id }).Select(c => new v_tag_trajectory_obj_vm
                {
                    unique_id = c.Key.unique_id,
                    tag_name = c.Key.tag_name,
                    region_guid = c.Key.region_guid,
                    device_id = c.Key.device_id,
                    status = CommonEnum.Status.Normal,
                    trajectory = new List<v_tag_trajectory_detail_vm>()
                }).ToList();
                var data_trajectory = query.ToList().Select(_mapper.Map<v_tag_trajectory, v_tag_trajectory_list_vm>);
                foreach (v_tag_trajectory_list_vm trajectory in data_trajectory)
                {
                    for (int i = 0; i < tag_data.Count; i++)
                    {
                        if (trajectory.unique_id == tag_data[i].unique_id && trajectory.region_guid == tag_data[i].region_guid)
                        {
                            tag_data[i].trajectory.Add(new v_tag_trajectory_detail_vm
                            {
                                position_top = trajectory.position_top,
                                position_left = trajectory.position_left,
                                position_x = trajectory.position_x,
                                position_y = trajectory.position_y,
                                position_z = trajectory.position_z,
                                create_time = trajectory.create_time,
                                create_millisecond = trajectory.create_millisecond,
                            });
                            break;
                        }
                    }
                            
                }
                var region_data = query.GroupBy(c => new { c.region_guid, c.region_name,c.region_parent_guid }).Select(c => new region_tree_vm
                {
                    Title = c.Key.region_name,
                    Guid = c.Key.region_guid.ToString(),// Guid.Empty.ToString(),
                    ParentGuid = c.Key.region_parent_guid.ToString()
                }).ToList();
                //var root = new region_tree_vm
                //{
                //    Title = "顶级名称",
                //    Guid = "0",// Guid.Empty.ToString(),
                //    ParentGuid = null
                //};
                //region_data.Insert(0, root);
                //var region_data = RegionTreeHelper.BuildTree(list_region,null);

                var response = ResponseModelFactory.CreateInstance;

                response.SetData(new { tag_data, region_data });
                return Ok(response);
            }
        }
        #endregion

    }
}
