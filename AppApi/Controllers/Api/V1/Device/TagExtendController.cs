﻿using AutoMapper;
using System.Linq;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload;
using AppApi.RequestPayload.Device;
using AppApi.ViewModels.Device;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;

namespace AppApi.Controllers.Api.V1.Device
{
    [Route("api/v1/device/[controller]/[action]")]
    [ApiController]
    public class TagExtendController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public TagExtendController(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        #region  Method
        [HttpPost]
        public IActionResult List(device_tag_rq payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.v_tag_extend.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.unique_id.Contains(payload.Kw.Trim()) || x.tag_name.Contains(payload.Kw.Trim()) || x.region_name.Contains(payload.Kw.Trim()));
                }
                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.status == payload.Status);
                }

                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "ASC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).ToList();
                var totalCount = query.Count();
                var data = list.Select(_mapper.Map<v_tag_extend, v_tag_extend_list_vm>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }
        [HttpPost]
        public IActionResult sel_tag(tag_etend_rq payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.v_tag_extend.AsQueryable();
                query = query.Where(x => x.region_guid == payload.region_guid);
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.unique_id.Contains(payload.Kw.Trim()) || x.tag_name.Contains(payload.Kw.Trim()));
                }
                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.status == payload.Status);
                }
                var data = query.ToList().Select(_mapper.Map<v_tag_extend, v_tag_extend_list_vm>);
                var response = ResponseModelFactory.CreateInstance;
                response.SetData(data);
                return Ok(response);
            }
        }
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(int id)
        {
            using (_dbContext)
            {
                var entity = _dbContext.v_tag_extend.FirstOrDefault(x => x.id == id);
                var response = ResponseModelFactory.CreateInstance;
                response.SetData(_mapper.Map<v_tag_extend, v_tag_extend_list_vm>(entity));
                return Ok(response);
            }
        }

        #endregion

    }
}
