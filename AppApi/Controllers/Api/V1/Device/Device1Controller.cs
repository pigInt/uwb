﻿using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using AppApi.Entities;
using AppApi.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload.Device;
using AppApi.ViewModels.Device.Device;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;
using AppApi.Entities.QueryModels;

namespace AppApi.Controllers.Api.V1.Device
{
    [Route("api/v1/device1/[controller]/[action]")]
    [ApiController]
    public class Device1Controller : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public Device1Controller(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        [HttpPost]
        public IActionResult List(DeviceRequestPayload payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.settings.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.Name.Contains(payload.Kw.Trim()) || x.Value.Contains(payload.Kw.Trim()));
                }
                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.Status == payload.Status);
                }

                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "DESC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).ToList();
                var totalCount = query.Count();
                var data = list.Select(_mapper.Map<settings, device_tree_model>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }
        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Create(device_create_model model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (model.DICode.Trim().Length <= 0)
            {
                response.SetFailed("请输入编号");
                return Ok(response);
            }
            if (model.DIName.Trim().Length <= 0)
            {
                response.SetFailed("请输入名称");
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.dnc_device.Count(x => x.DICode == model.DICode) > 0)
                {
                    response.SetFailed("设备已存在");
                    return Ok(response);
                }

                var entity = _mapper.Map<device_create_model, dnc_device>(model);
                entity.DICode = model.DICode;
                entity.remote_code = model.DICode.Substring(8, 2);
                entity.site_code = model.DICode.Substring(0, 8);
                entity.DIName = model.DIName;
                entity.Status = 9;
                entity.IsDeleted = 0;
                entity.parent_code = model.parent_code;
                entity.DItype = model.DItype;
                entity.citycode = model.citycode;
                entity.zonecode = model.zonecode;
                entity.CreateDate = DateTime.Now;
                entity.ModDate = DateTime.Now;
                _dbContext.dnc_device.Add(entity);
                _dbContext.SaveChanges();

                response.SetSuccess();
                return Ok(response);
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(int id)
        {
            using (_dbContext)
            {
                var entity = _dbContext.dnc_device.FirstOrDefault(x => x.DIid == id);
                var response = ResponseModelFactory.CreateInstance;
                response.SetData(_mapper.Map<dnc_device, device_edit_model>(entity));
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Edit(device_edit_model model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.dnc_device.Count(x => x.DICode == model.DICode && x.DIid != model.DIid) > 0)
                {
                    response.SetFailed("设备名称已存在");
                    return Ok(response);
                }
                var entity = _dbContext.dnc_device.FirstOrDefault(x => x.DIid == model.DIid);
                entity.DICode = model.DICode;
                entity.DIName = model.DIName;
                entity.Status = model.Status;
                //entity.IsDeleted = 0;
                entity.DItype = model.DItype;
                //entity.ModDate = DateTime.Now;
                entity.citycode = model.citycode;
                entity.zonecode = model.zonecode;
                _dbContext.SaveChanges();
                response = ResponseModelFactory.CreateInstance;
                return Ok(response);
            }
        }
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Delete(string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            response = UpdateIsDelete(1, ids);
            return Ok(response);
        }
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Recover(string ids)
        {
            var response = UpdateIsDelete(0, ids);
            return Ok(response);
        }
        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Batch(string command, string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            switch (command)
            {
                case "delete":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateIsDelete(1, ids);
                    break;
                case "recover":
                    response = UpdateIsDelete(0, ids);
                    break;
                case "forbidden":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateStatus(2, ids);
                    break;
                case "normal":
                    response = UpdateStatus(0, ids);
                    break;
                default:
                    break;
            }
            return Ok(response);
        }

        /// <summary>
        /// 省市地区树
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/v1/device/device/city_tree")]
        public IActionResult city_tree()
        {
            var response = ResponseModelFactory.CreateInstance;
            using (_dbContext)
            {
                var sql = @"select * from dnc_city";              // Union All select '',  
                var citys = _dbContext.dnc_city.FromSql(sql).ToList();
                sql = @"select * from dnc_zonearea";
                var zoneareas = _dbContext.dnc_zonearea.FromSql(sql).ToList();
                var tree = DeviceTreeHelper1.fill_city(citys, zoneareas);
                response.SetData(tree);
            }

            return Ok(response);
        }

        /// <summary>
        /// 设备列表树
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult device_tree(device_tree_request payload)
        {
            var response = ResponseModelFactory.CreateInstance;
            using (_dbContext)
            {
                var query = _dbContext.dnc_device.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.DIName.Contains(payload.Kw.Trim()) || x.DICode.Contains(payload.Kw.Trim()));
                }
                if (payload.IsDeleted > -1)
                {
                    query = query.Where(x => x.IsDeleted == payload.IsDeleted);
                }
                if (payload.Status > -1)
                {
                    query = query.Where(x => x.Status == payload.Status);
                }
                if (payload.citycode != "")
                {
                    //sql = @"select * from dnc_device where citycode={0} and zonecode={1}";
                    query = query.Where(x => x.citycode == payload.citycode);

                }
                if (payload.zonecode != "")
                {
                    //sql = @"select * from dnc_device where citycode={0} and zonecode={1}";
                    query = query.Where(x => x.zonecode == payload.zonecode);

                }
                // else if (citycode != "")
                {
                    //sql = @"select * from dnc_device where citycode={0}";
                }
                //if (zonecode != "" )
                {
                    //sql = @"select * from dnc_device where zonecode={1}";
                }
                //var devices = _dbContext.dnc_device.FromSql(sql, citycode, zonecode).ToList();
                var tree = DeviceTreeHelper1.fill_device(query.ToList(), "0", "0");
                response.SetData(tree);
            }

            return Ok(response);
        }

        /// <summary>
        /// 设备列表树
        /// </summary>
        /// <returns></returns>
        [HttpPost("/api/v1/device/device/parent")]
        public IActionResult find_parent(device_tree_request payload)
        {
            var response = ResponseModelFactory.CreateInstance;
            using (_dbContext)
            {
                var query = _dbContext.dnc_device.AsQueryable().Where(x => x.DItype == "0");
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.DIName.Contains(payload.Kw.Trim()) || x.DICode.Contains(payload.Kw.Trim()));
                }
                if (payload.IsDeleted > -1)
                {
                    query = query.Where(x => x.IsDeleted == payload.IsDeleted);
                }
                if (payload.Status > -1)
                {
                    query = query.Where(x => x.Status == payload.Status);
                }
                if (payload.citycode != "")
                {
                    query = query.Where(x => x.citycode == payload.citycode);

                }
                if (payload.zonecode != "")
                {
                    query = query.Where(x => x.zonecode == payload.zonecode);
                }
                var list = query.ToList();
                var data = list.Select(x => new { x.DIid, x.DICode, x.DIName });
                response.SetData(data);
            }

            return Ok(response);
        }

        #region 私有方法
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <param name="ids">用户ID字符串,多个以逗号隔开</param>
        /// <returns></returns>
        private ResponseModel UpdateIsDelete(int isDeleted, string ids)
        {
            using (_dbContext)
            {
                //var idList = ids.Split(",").ToList();
                ////idList.ForEach(x => {
                ////  _dbContext.Database.ExecuteSqlCommand($"UPDATE DncUser SET IsDeleted=1 WHERE Id = {x}");
                ////});
                //_dbContext.Database.ExecuteSqlCommand($"UPDATE DncUser SET IsDeleted={(int)isDeleted} WHERE Id IN ({ids})");
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("UPDATE dnc_device SET IsDeleted=@IsDeleted WHERE DIid IN ({0})", parameterNames);
                parameters.Add(new MySqlParameter("@IsDeleted", (int)isDeleted));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }

        private ResponseModel UpdateStatus(int status, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("UPDATE dnc_device SET Status=@Status WHERE DIid IN ({0})", parameterNames);
                parameters.Add(new MySqlParameter("@Status", (int)status));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }
        #endregion
    }
    public static class DeviceTreeHelper1
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="citys"></param>
        /// <param name="zonearea"></param>
        /// <returns></returns>
        public static List<city_tree_model> fill_city(List<dnc_city> citys, List<dnc_zonearea> zonearea)
        {
            List<city_tree_model> recursiveObjects = new List<city_tree_model>();
            foreach (var item in citys)
            {
                var children = new city_tree_model
                {
                    Expand = true,
                    Code = item.citycode,
                    Title = item.cityname,
                    Children = fill_zonearea(zonearea, item.zid, item.citycode)
                };
                recursiveObjects.Add(children);
            }
            return recursiveObjects;
        }
        public static List<city_tree_model> fill_zonearea(List<dnc_zonearea> zonearea, int zid, string citycode)
        {
            List<city_tree_model> recursiveObjects = new List<city_tree_model>();
            foreach (var item in zonearea.Where(x => x.zid == zid))
            {
                var children = new city_tree_model
                {
                    Expand = true,
                    ParentId = citycode,
                    Code = item.zonecode,
                    Title = item.zonename,
                };
                recursiveObjects.Add(children);
            }
            return recursiveObjects;
        }

        public static List<device_tree_model> fill_device(List<dnc_device> devices, string device_code, string ditype)
        {
            List<device_tree_model> recursiveObjects = new List<device_tree_model>();
            foreach (var item in devices.Where(x => x.parent_code == device_code))
            {
                var children = new device_tree_model();
                if (ditype == "0")
                {
                    children = new device_tree_model
                    {
                        Expand = true,
                        DIid = item.DIid,
                        DICode = item.DICode,
                        DIName = item.DIName,
                        DItype = item.DItype,
                        Status = item.Status,
                        CreateDate = item.CreateDate,
                        parent_code = item.parent_code,
                        citycode = item.citycode,
                        zonecode = item.zonecode,
                        Children = fill_device(devices, item.DICode, item.DItype)
                    };
                }
                else
                {
                    children = new device_tree_model
                    {
                        Expand = true,
                        DIid = item.DIid,
                        DICode = item.DICode,
                        DIName = item.DIName,
                        DItype = item.DItype,
                        Status = item.Status,
                        CreateDate = item.CreateDate,
                        parent_code = item.parent_code,
                        citycode = item.citycode,
                        zonecode = item.zonecode
                    };

                }
                recursiveObjects.Add(children);
            }
            return recursiveObjects;
        }
    }
}

