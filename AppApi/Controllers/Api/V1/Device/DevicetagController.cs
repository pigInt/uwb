﻿using AutoMapper;
using System.Linq;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload.Device;
using AppApi.ViewModels.Device;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;

namespace AppApi.Controllers.Api.V1.Device
{
    [Route("api/v1/device/[controller]/[action]")]
    [ApiController]
    public class DeviceTagController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public DeviceTagController(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        #region  Method
        [HttpPost]
        public IActionResult List(device_tag_rq payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.device_tag.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.code.Contains(payload.Kw.Trim()) || x.unique_id.Contains(payload.Kw.Trim()) || x.name.Contains(payload.Kw.Trim()));
                }
                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.status == payload.Status);
                }

                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "DESC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).ToList();
                var totalCount = query.Count();
                var data = list.Select(_mapper.Map<device_tag, device_tag_list_vm>);
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Create(device_tag_create_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;

            if (model.unique_id =="")
            {
                response.SetFailed("请输入标签KEY");
                return Ok(response);
            }
            if (model.code.Trim().Length <= 0)
            {
                response.SetFailed("请输入标签编号");
                return Ok(response);
            }

            using (_dbContext)
            {
                if (_dbContext.device_tag.Count(x => x.unique_id == model.unique_id) > 0)
                {
                    response.SetFailed("标签已存在");
                    return Ok(response);
                }

                var entity = _mapper.Map<device_tag_create_vm, device_tag>(model);
                entity.id = model.id;
                entity.unique_id = model.unique_id;
                entity.name = model.name;
                entity.code = model.code;
                entity.status = model.status;
                entity.type = model.type;
                entity.electricity = model.electricity;
                entity.create_time = DateTime.Now;
                entity.modify_time = DateTime.Now;
                _dbContext.device_tag.Add(entity);
                _dbContext.SaveChanges();

                response.SetSuccess();
                return Ok(response);
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(int id)
        {
            using (_dbContext)
            {
                var entity = _dbContext.device_tag.FirstOrDefault(x => x.id == id);
                var response = ResponseModelFactory.CreateInstance;
                response.SetData(_mapper.Map<device_tag, device_tag_edit_vm>(entity));
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Edit(device_tag_edit_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.device.Count(x => x.code == model.code && x.id != model.id) > 0)
                {
                    response.SetFailed("标签名称已存在");
                    return Ok(response);
                }
                var entity = _dbContext.device_tag.FirstOrDefault(x => x.id == model.id);
                entity.id = model.id;
                entity.unique_id = model.unique_id;
                entity.name = model.name;
                entity.code = model.code;
                entity.status = model.status;
                entity.type = model.type;
                entity.electricity = model.electricity;
                entity.modify_time = DateTime.Now;
                _dbContext.SaveChanges();
                response = ResponseModelFactory.CreateInstance;
                return Ok(response);
            }
        }

        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Delete(string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            response = UpdateIsDelete(1, ids);
            return Ok(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Recover(string ids)
        {
            var response = UpdateIsDelete(0, ids);
            return Ok(response);
        }

        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Batch(string command, string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            switch (command)
            {
                case "delete":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateIsDelete(1, ids);
                    break;
                case "recover":
                    response = UpdateIsDelete(0, ids);
                    break;
                case "forbidden":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateStatus(2, ids);
                    break;
                case "normal":
                    response = UpdateStatus(0, ids);
                    break;
                default:
                    break;
            }
            return Ok(response);
        }

        #endregion

        #region 私有方法
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <param name="ids">用户ID字符串,多个以逗号隔开</param>
        /// <returns></returns>
        private ResponseModel UpdateIsDelete(int isDeleted, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("delete from device_tag WHERE id IN ({0})", parameterNames);
                //var sql = string.Format("UPDATE label SET IsDeleted=@IsDeleted WHERE DIid IN ({0})", parameterNames);
                //parameters.Add(new MySqlParameter("@IsDeleted", (int)isDeleted));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }

        private ResponseModel UpdateStatus(int status, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("UPDATE device_tag SET status=@Status WHERE id IN ({0})", parameterNames);
                parameters.Add(new MySqlParameter("@Status", (int)status));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }
        #endregion
    }
}
