﻿using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using AppEntity.Entities;
using AppEntity.Entities.Enums;
using AppApi.Extensions;
using AppApi.Extensions.DataAccess;
using AppApi.RequestPayload.Device;
using AppApi.ViewModels.Device;
using Microsoft.AspNetCore.Mvc;
using System;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using AppApi.Models.Response;
using AppEntity.Entities.QueryModels;

namespace AppApi.Controllers.Api.V1.Device
{
    [Route("api/v1/Device/[controller]/[action]")]
    [ApiController]
    public class DeviceAlarmController : ControllerBase
    {
        private readonly DncZeusDbContext _dbContext;
        private readonly IMapper _mapper;

        public DeviceAlarmController(DncZeusDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        #region  Method
        [HttpPost]
        public IActionResult List(device_alarm_rq payload)
        {
            using (_dbContext)
            {
                var query = _dbContext.device_alarm.AsQueryable();
                if (!string.IsNullOrEmpty(payload.Kw))
                {
                    query = query.Where(x => x.device.name.Contains(payload.Kw.Trim()) || x.device.code.Contains(payload.Kw.Trim()));
                }
                if (payload.Status > CommonEnum.Status.All)
                {
                    query = query.Where(x => x.status == payload.Status);
                }

                if (payload.FirstSort != null)
                {
                    query = query.OrderBy(payload.FirstSort.Field, payload.FirstSort.Direct == "DESC");
                }
                var list = query.Paged(payload.CurrentPage, payload.PageSize).Include(x => x.device).ToList();
                var totalCount = query.Count();
                //var data = list.Select(_mapper.Map<device_alarm, device_alarm_list_vm>);
                var data = list.Select(x => new { device_name = x.device.name, device_mac = x.device.mac_id,x.id, x.device_id, x.status, alarm_time= x.alarm_time.ToString(), recover_time = x.recover_time.ToString(), x.alarm_level, x.alarm_num});
                var response = ResponseModelFactory.CreateResultInstance;
                response.SetData(data, totalCount);
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Create(device_alarm_create_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;


            using (_dbContext)
            {
                if (_dbContext.device_alarm.Count(x => x.device_id == model.device_id && x.id == model.id) > 0)
                {
                    response.SetFailed("告警已存在");
                    return Ok(response);
                }

                var entity = _mapper.Map<device_alarm_create_vm, device_alarm>(model);
                entity.device_id = model.device_id;
                entity.status = model.status;
                entity.alarm_time = DateTime.Now;
                entity.alarm_level = model.alarm_level;
                entity.alarm_num = model.alarm_num;
                _dbContext.device_alarm.Add(entity);
                _dbContext.SaveChanges();

                response.SetSuccess();
                return Ok(response);
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public IActionResult Edit(int id)
        {
            using (_dbContext)
            {
                var entity = _dbContext.device_alarm.FirstOrDefault(x => x.id == id);
                var response = ResponseModelFactory.CreateInstance;
                response.SetData(_mapper.Map<device_alarm, device_alarm_edit_vm>(entity));
                return Ok(response);
            }
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public IActionResult Edit(device_alarm_edit_vm model)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            using (_dbContext)
            {
                if (_dbContext.device_alarm.Count(x => x.device_id == model.device_id && x.id != model.id) > 0)
                {
                    response.SetFailed("告警已存在");
                    return Ok(response);
                }
                var entity = _dbContext.device_alarm.FirstOrDefault(x => x.id == model.id);
                entity.device_id = model.device_id;
                entity.status = model.status;
                entity.recover_time = DateTime.Now;
                entity.alarm_level = model.alarm_level;
                entity.alarm_num = model.alarm_num;
                _dbContext.SaveChanges();
                response = ResponseModelFactory.CreateInstance;
                return Ok(response);
            }
        }

        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Delete(string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            if (ConfigurationManager.AppSettings.IsTrialVersion)
            {
                response.SetIsTrial();
                return Ok(response);
            }
            response = UpdateIsDelete(1, ids);
            return Ok(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet("{ids}")]
        [ProducesResponseType(200)]
        public IActionResult Recover(string ids)
        {
            var response = UpdateIsDelete(0, ids);
            return Ok(response);
        }

        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ids">ID,多个以逗号分隔</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200)]
        public IActionResult Batch(string command, string ids)
        {
            var response = ResponseModelFactory.CreateInstance;
            switch (command)
            {
                case "delete":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateIsDelete(1, ids);
                    break;
                case "recover":
                    response = UpdateIsDelete(0, ids);
                    break;
                case "forbidden":
                    if (ConfigurationManager.AppSettings.IsTrialVersion)
                    {
                        response.SetIsTrial();
                        return Ok(response);
                    }
                    response = UpdateStatus(0, ids);
                    break;
                case "normal":
                    response = UpdateStatus(1, ids);
                    break;
                default:
                    break;
            }
            return Ok(response);
        }

        #endregion

        #region 私有方法
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <param name="ids">用户ID字符串,多个以逗号隔开</param>
        /// <returns></returns>
        private ResponseModel UpdateIsDelete(int isDeleted, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("delete from device_alarm WHERE id IN ({0})", parameterNames);
                //var sql = string.Format("UPDATE device_alarm SET IsDeleted=@IsDeleted WHERE DIid IN ({0})", parameterNames);
                //parameters.Add(new MySqlParameter("@IsDeleted", (int)isDeleted));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }

        private ResponseModel UpdateStatus(int status, string ids)
        {
            using (_dbContext)
            {
                var parameters = ids.Split(",").Select((id, index) => new MySqlParameter(string.Format("@p{0}", index), id)).ToList();
                var parameterNames = string.Join(", ", parameters.Select(p => p.ParameterName));
                var sql = string.Format("UPDATE device_alarm SET status=@status WHERE id IN ({0})", parameterNames);
                parameters.Add(new MySqlParameter("@status", (int)status));
                _dbContext.Database.ExecuteSqlCommand(sql, parameters);
                var response = ResponseModelFactory.CreateInstance;
                return response;
            }
        }
        #endregion
    }
}
