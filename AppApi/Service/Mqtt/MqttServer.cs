﻿
using MQTTnet;
using MQTTnet.Server;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MQTTnet.Client.Receiving;
using System.Text;
using System.IO;

namespace AppApi.Service.Mqtt
{
    public class MqttServer
    {
        public static IMqttServer mqttServer;
        private static int port = 0;
        private static List<ClientInfos> clientinfos = null;

        public static void Start()
        {
            try
            {
                Task.Run(async () =>
                {

                    await StartMqttServer();
                });
                //Thread.Sleep(Timeout.Infinite);

            }
            catch (Exception ex)
            {
                Console.WriteLine("启动客户端出现问题:" + ex.ToString());
            }
        }
        //启动Mqtt服务器
        private static async Task StartMqttServer()
        {
            InitBaseData();
            if (mqttServer == null)
            {
                try
                {

                    //验证客户端信息
                    var options = new MqttServerOptions()
                    {
                        ConnectionValidator = new MqttServerConnectionValidatorDelegate(p =>
                       {
                           var client = clientinfos.FindAll(c => c.clientId == p.ClientId && c.userName == p.Username && c.password == p.Password);
                           if (client.Count == 0)
                           {
                               Console.WriteLine("账号密码错误");
                               p.ReasonCode = MQTTnet.Protocol.MqttConnectReasonCode.BadUserNameOrPassword;
                           }
                           //if (p.Username != "admin" && p.Password != "123456")
                           //{
                           //    Console.WriteLine("账号密码错误");
                           //    return MQTTnet.Core.Protocol.MqttConnectReturnCode.ConnectionRefusedBadUsernameOrPassword;
                           //}
                           p.ReasonCode = MQTTnet.Protocol.MqttConnectReasonCode.Success;
                       }),
                        //消息拦截发送验证
                        ApplicationMessageInterceptor = new MqttServerApplicationMessageInterceptorDelegate(context =>
                        {
                            if (MqttTopicFilterComparer.IsMatch(context.ApplicationMessage.Topic, "/myTopic/WithTimestamp/#"))
                            {
                                // Replace the payload with the timestamp. But also extending a JSON 
                                // based payload with the timestamp is a suitable use case.
                                context.ApplicationMessage.Payload = Encoding.UTF8.GetBytes(DateTime.Now.ToString("O"));
                            }

                            if (context.ApplicationMessage.Topic == "not_allowed_topic")
                            {
                                context.AcceptPublish = false;
                                context.CloseConnection = true;
                            }
                        }),
                        ///订阅拦截验证
                        SubscriptionInterceptor = new MqttServerSubscriptionInterceptorDelegate(context =>
                        {
                            if (context.TopicFilter.Topic.StartsWith("admin/foo/bar") && context.ClientId != "theAdmin")
                            {
                                context.AcceptSubscription = false;
                            }

                            if (context.TopicFilter.Topic.StartsWith("the/secret/stuff") && context.ClientId != "Imperator")
                            {
                                context.AcceptSubscription = false;
                                context.CloseConnection = true;
                            }
                        })

                    };
                    options.DefaultEndpointOptions.Port = Startup.mqttSettings.HostPort;//mqtt通信端口设置
                    mqttServer.ApplicationMessageReceivedHandler = new MqttApplicationMessageReceivedHandlerDelegate(e =>
                    {
                        Console.WriteLine(
                            $"'{e.ClientId}' reported '{e.ApplicationMessage.Topic}' > '{Encoding.UTF8.GetString(e.ApplicationMessage.Payload ?? new byte[0])}'",
                            ConsoleColor.Magenta);
                    });
                    //开启订阅事件
                    mqttServer.ClientSubscribedTopicHandler = new MqttServerClientSubscribedHandlerDelegate(new Action<MqttServerClientSubscribedTopicEventArgs>(MqttNetServer_SubscribedTopic));
                    //取消订阅事件
                    mqttServer.ClientUnsubscribedTopicHandler = new MqttServerClientUnsubscribedTopicHandlerDelegate(new Action<MqttServerClientUnsubscribedTopicEventArgs>(MqttNetServer_UnSubscribedTopic));

                    //客户端消息事件
                    mqttServer.UseApplicationMessageReceivedHandler(new Action<MqttApplicationMessageReceivedEventArgs>(MqttServe_ApplicationMessageReceived));

                    //客户端连接事件
                    mqttServer.UseClientConnectedHandler(new Action<MqttServerClientConnectedEventArgs>(MqttNetServer_ClientConnected));

                    //客户端断开事件
                    mqttServer.UseClientDisconnectedHandler(new Action<MqttServerClientDisconnectedEventArgs>(MqttNetServer_ClientDisConnected));

                    //启动服务器
                    await mqttServer.StartAsync(options);

                    Console.WriteLine("服务器启动成功！输入任意内容并回车停止服务！");
                    Console.ReadLine();

                    await mqttServer.StopAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("连接mqtt失败");
                    return;
                }
            }

        }
        private static void InitBaseData()
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("ServerInfo.json");
            var config = builder.Build();
            port = int.Parse(config["port"]);
            int count = int.Parse(config["clientsCount"]);
            clientinfos = new List<ClientInfos>();
            for (int i = 0; i < count; i++)
            {
                var clientinfo = new ClientInfos
                {
                    clientId = config[$"clientInfo:{i}:clientId"],
                    userName = config[$"clientInfo:{i}:userName"],
                    password = config[$"clientInfo:{i}:password"]
                };
                clientinfos.Add(clientinfo);
            }
        }
        //客户订阅
        private static void MqttNetServer_SubscribedTopic(MqttServerClientSubscribedTopicEventArgs e)
        {
            //客户端Id
            var ClientId = e.ClientId;
            var Topic = e.TopicFilter.Topic;
            Console.WriteLine($"客户端[{ClientId}]已订阅主题：{Topic}");
        }

        //客户取消订阅
        private static void MqttNetServer_UnSubscribedTopic(MqttServerClientUnsubscribedTopicEventArgs e)
        {
            //客户端Id
            var ClientId = e.ClientId;
            var Topic = e.TopicFilter;
            Console.WriteLine($"客户端[{ClientId}]已取消订阅主题：{Topic}");
        }

        //接收消息
        private static void MqttServe_ApplicationMessageReceived(MqttApplicationMessageReceivedEventArgs e)
        {
            var ClientId = e.ClientId;
            var Topic = e.ApplicationMessage.Topic;
            var Payload = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
            var Qos = e.ApplicationMessage.QualityOfServiceLevel;
            var Retain = e.ApplicationMessage.Retain;
            Console.WriteLine($"客户端[{ClientId}]>> 主题：[{Topic}] 负载：[{Payload}] Qos：[{Qos}] 保留：[{Retain}]");
        }

        //客户连接
        private static void MqttNetServer_ClientConnected(MqttServerClientConnectedEventArgs e)
        {
            var ClientId = e.ClientId;
            Console.WriteLine($"客户端[{ClientId}]已连接");
        }

        //客户连接断开
        private static void MqttNetServer_ClientDisConnected(MqttServerClientDisconnectedEventArgs e)
        {
            var ClientId = e.ClientId;
            Console.WriteLine($"客户端[{ClientId}]已断开连接");
        }
    }
    public class ClientInfos
    {
        public string clientId { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
    }
}
