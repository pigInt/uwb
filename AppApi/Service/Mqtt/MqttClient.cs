﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using MQTTnet;
using MQTTnet.AspNetCore;
using MQTTnet.Client;
using MQTTnet.Client.Options;
using MQTTnet.Protocol;
using MQTTnet.Client.Receiving;　　　　//接收
using MQTTnet.Client.Disconnecting;　　//断线
using MQTTnet.Client.Connecting;　　　　//连接
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AppApi;
using Microsoft.Extensions.Logging;
using AppCore;

namespace AppApi.Service.Mqtt
{
    public class MqttClient
    {
        private static ILogger _logger;
        private static IMqttClient mqttClient = null;
        private static IMqttClientOptions options = null;
        public static bool runState = false;
        private static bool running = false;
        /// <summary>        
        /// 主题        
        /// <para>Device/Attr</para>        
        /// <para>Device/#</para>        
        /// </summary>  
        private static string Topic = "demo/iot_test01/device10";
        /// <summary>        
        /// 保留        
        /// </summary>        
        private static bool Retained = false;
        /// <summary>       
        /// 服务质量        
        /// <para>0 - 至多一次</para>        
        /// <para>1 - 至少一次</para>        
        /// <para>2 - 刚好一次</para>        
        /// </summary>        
        private static int QualityOfServiceLevel = 0;
        private static string clientId = "SystemId";
        public MqttClient(ILogger logger)
        {
           
            _logger = logger;
        }
        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            try
            {
                runState = true;
                Thread thread = new Thread(Work);　　　　//原帖中是这样写的 Thread thread = new Thread(new ThreadStart( Work));
                thread.IsBackground = true;
                thread.Start();
                _logger.Log(LogLevel.Information, $"MqttClient Started");
            }
            catch (Exception ex)
            {
                Console.WriteLine("启动客户端出现问题:" + ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private static void Work()
        {
            //LogHelper.Info("dddddddddddddddddddddddddd");
            MqttClient._logger.Log(LogLevel.Information, $"MqttClient Work Begin");
            running = true;
            //_logger.Log(LogLevel.Information,"Work  Begin");
            var mqttSettings = Startup.mqttSettings;

            try
            {
                if (mqttClient == null)
                {
                    var factory = new MqttFactory();
                    mqttClient = factory.CreateMqttClient();  //factory.CreateMqttClient()实际是一个接口类型（IMqttClient）,这里是把他的类型变了一下
                    mqttClient.ConnectedHandler = new MqttClientConnectedHandlerDelegate(new Func<MqttClientConnectedEventArgs, Task>(Connected));
                    mqttClient.DisconnectedHandler = new MqttClientDisconnectedHandlerDelegate(new Func<MqttClientDisconnectedEventArgs, Task>(Disconnected));
                    mqttClient.ApplicationMessageReceivedHandler = new MqttApplicationMessageReceivedHandlerDelegate(new Action<MqttApplicationMessageReceivedEventArgs>(MqttApplicationMessageReceived));
                }
                options = new MqttClientOptionsBuilder()　　　　//实例化一个MqttClientOptionsBulider
                    .WithTcpServer(mqttSettings.HostIp, mqttSettings.HostPort)
                    .WithCredentials(mqttSettings.UserName, mqttSettings.Password)
                    .WithClientId(clientId)
                    .Build();
                mqttClient.ConnectAsync(options);      //连接服务器

                while (runState)
                {
                    Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, $"连接失败：{ex.Message}");
            }
            _logger.Log(LogLevel.Information, "Work >>End");
            running = false;
            runState = false;
        }

        private static async Task Connected(MqttClientConnectedEventArgs e)
        {
            try
            {
                List<TopicFilter> listTopic = new List<TopicFilter>();
                if (listTopic.Count() <= 0)
                {
                    var topicFilterBulder = new TopicFilterBuilder().WithTopic(Topic).Build();
                    listTopic.Add(topicFilterBulder);
                    Console.WriteLine("Connected >>Subscribe " + Topic);
                }
                await mqttClient.SubscribeAsync(listTopic.ToArray());
                _logger.Log(LogLevel.Information, "Connected >>Subscribe Success");
            }
            catch (Exception exp)
            {
                _logger.Log(LogLevel.Error, exp.Message);
            }
        }

        private static async Task Disconnected(MqttClientDisconnectedEventArgs e)
        {
            try
            {
                Console.WriteLine("Disconnected >>Disconnected Server");
                await Task.Delay(TimeSpan.FromSeconds(5));
                try
                {
                    await mqttClient.ConnectAsync(options);
                }
                catch (Exception exp)
                {
                    Console.WriteLine("Disconnected >>Exception " + exp.Message);
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }
        }

        /// <summary>
        /// 接收消息触发事件
        /// </summary>
        /// <param name="e"></param>
        private static void MqttApplicationMessageReceived(MqttApplicationMessageReceivedEventArgs e)
        {
            try
            {
                string text = Encoding.UTF8.GetString(e.ApplicationMessage.Payload);
                string Topic = e.ApplicationMessage.Topic;
                string QoS = e.ApplicationMessage.QualityOfServiceLevel.ToString();
                string Retained = e.ApplicationMessage.Retain.ToString();
                switch (Topic)
                {
                    case "gateway/attribute":
                        {
                            var request = JsonConvert.DeserializeObject<ViewModels.Gateway.attribute_model>(text).ToString();
                            //new AppApi.Controllers.Api.V1.Gateway.GatewayController().Attribute(request);
                            break;
                        }
                    case "Device/Attr1":
                        {
                            break;
                        }
                }
                _logger.Log(LogLevel.Information, "MessageReceived >>Topic:" + Topic + "; QoS: " + QoS + "; Retained: " + Retained + ";");
                _logger.Log(LogLevel.Information, "MessageReceived >>Msg: " + text);
            }
            catch (Exception exp)
            {
                _logger.Log(LogLevel.Error, exp.Message);
            }
        }
        /// <summary>       
        /// /// 发布        
        /// <paramref name="QoS"/>        
        /// <para>0 - 最多一次</para>        
        /// <para>1 - 至少一次</para>        
        /// <para>2 - 仅一次</para>        
        /// </summary>       
        /// <param name="Topic">发布主题</param>        
        /// <param name="Message">发布内容</param>        
        /// <returns></returns>        
        public static void Publish(string Topic, string Message)
        {
            try
            {
                if (mqttClient == null)
                    return;
                if (mqttClient.IsConnected == false)
                    mqttClient.ConnectAsync(options);
                if (mqttClient.IsConnected == false)
                {
                    Console.WriteLine("Publish >>Connected Failed! ");
                    return;
                }
                Console.WriteLine("Publish >>Topic: " + Topic + "; QoS: " + QualityOfServiceLevel + "; Retained: " + Retained + ";");
                Console.WriteLine("Publish >>Message: " + Message);
                MqttApplicationMessageBuilder mamb = new MqttApplicationMessageBuilder()
                    .WithTopic(Topic)
                    .WithPayload(Message).WithRetainFlag(Retained);
                if (QualityOfServiceLevel == 0)
                {
                    mamb = mamb.WithAtMostOnceQoS();
                }
                else if (QualityOfServiceLevel == 1)
                {
                    mamb = mamb.WithAtLeastOnceQoS();
                }
                else if (QualityOfServiceLevel == 2)
                {
                    mamb = mamb.WithExactlyOnceQoS();
                }
                mqttClient.PublishAsync(mamb.Build());
            }
            catch (Exception exp)
            {
                Console.WriteLine("Publish >>" + exp.Message);
            }
        }

        /*
        private void Something()
        {
            mqttClient.ApplicationMessageReceivedHandler = new MqttApplicationMessageReceivedHandlerDelegate(OnAppMessage);
            mqttClient.ConnectedHandler = new MqttClientConnectedHandlerDelegate(OnConnected);
            mqttClient.DisconnectedHandler = new MqttClientDisconnectedHandlerDelegate(OnDisconnected);
        }

        private async void OnAppMessage(MqttApplicationMessageReceivedEventArgs e)
        {
        }

        private async void OnConnected(MqttClientConnectedEventArgs e)
        {
        }

        private async void OnDisconnected(MqttClientDisconnectedEventArgs e)
        {
        }
        */

    }
}
