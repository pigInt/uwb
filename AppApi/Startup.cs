﻿/******************************************
 * AUTHOR:          Rector
 * CREATEDON:       2018-09-26
 * OFFICIAL_SITE:    码友网(https://codedefault.com)--专注.NET/.NET Core
 * 版权所有，请勿删除
 ******************************************/

using AutoMapper;
using AppApi.Auth;
using AppEntity.Entities;
using AppApi.Extensions.AuthContext;
using AppApi.Extensions.CustomException;
using AppApi.Models.Mqtt;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.WebEncoders;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using MySql.Data.MySqlClient;


namespace AppApi
{
    public class Startup
    {
        private ILogger<Startup> _logger;
        public static MqttOptions mqttSettings = null;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
 
            
            //指定域名  允许任何来源的主机访问 builder.AllowAnyOrigin()
            services.AddCors(o =>
                o.AddPolicy("*",
                    builder => builder
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .SetIsOriginAllowed(_ => true)  //.AllowAnyOrigin()
                        .AllowCredentials()
                ));
               
            services.AddMemoryCache();
            services.AddHttpContextAccessor();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppAuthenticationSettings>(appSettingsSection);
            // JWT
            var appSettings = appSettingsSection.Get<AppAuthenticationSettings>();
            services.AddJwtBearerAuthentication(appSettings);
            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
            services.AddAutoMapper();

            services.Configure<WebEncoderOptions>(options =>
                options.TextEncoderSettings = new TextEncoderSettings(UnicodeRanges.BasicLatin, UnicodeRanges.CjkUnifiedIdeographs)
            );
           
            services
                .AddMvc(config =>
                {
                    //config.Filters.Add(new ValidateModelAttribute());
                })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            

            #region MQTT配置及连接
            
            var mqttOptionSection = Configuration.GetSection("MqttOptions");
            services.Configure<MqttOptions>(mqttOptionSection);
            mqttSettings = mqttOptionSection.Get<MqttOptions>();
            //构建配置项
            /*
            var optionBuilder = new MqttServerOptionsBuilder()
                .WithDefaultEndpointBoundIPAddress(System.Net.IPAddress.Parse(mqttSettings.HostIp))
                .WithDefaultEndpointPort(mqttSettings.HostPort)
                .WithDefaultCommunicationTimeout(TimeSpan.FromMilliseconds(mqttSettings.Timeout))
                .WithApplicationMessageInterceptor((context) =>
                {
                    //_logger.Log(LogLevel.Information, $"clientId:{context.ClientId}, topic:{context.ApplicationMessage.Topic}");
                    //_logger.Log(LogLevel.Information, $"Payload:{System.Text.Encoding.Default.GetString(context.ApplicationMessage.Payload)}");
                    Console.WriteLine($"clientId:{context.ClientId}, topic:{context.ApplicationMessage.Topic}");
                    string tmp_str = $"客户端[{context.ClientId}]>> 主题：{context.ApplicationMessage.Topic} 负荷：{System.Text.Encoding.UTF8.GetString(context.ApplicationMessage.Payload)} Qos：{context.ApplicationMessage.QualityOfServiceLevel} 保留：{context.ApplicationMessage.Retain}";
                    Console.WriteLine(tmp_str);

                })
                .WithConnectionValidator(t =>
                {
                    if (t.Username != mqttSettings.UserName || t.Password != mqttSettings.Password)
                    {
                        t.ReturnCode = MqttConnectReturnCode.ConnectionRefusedBadUsernameOrPassword;
                    }
                    t.ReturnCode = MqttConnectReturnCode.ConnectionAccepted;
                });
            var option = optionBuilder.Build();
            //MQTT服务注入
            services
                .AddHostedMqttServer(option)
                .AddMqttConnectionHandler()
                .AddConnections();
                */
            #endregion


            services.AddDbContext<DncZeusDbContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("DefaultConnection"))
                // 如果使用SQL Server 2008数据库，请添加UseRowNumberForPaging的选项
                // 参考资料:https://github.com/aspnet/EntityFrameworkCore/issues/4616
                //options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),b=>b.UseRowNumberForPaging())
             );

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "RBAC Management System API", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            // 注入日志
            services.AddLogging(config => 
            {
                config.AddLog4Net();
            });

            #region 注入注册托管服务
            ///加载服务
            services.AddHostedService<TaskService>();
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //app.UseDeveloperExceptionPage();
            //app.UseExceptionHandler("/error/500");
            //app.UseStatusCodePagesWithReExecute("/error/{0}");
            //app.UseMvc();




            #region MTQQ事件
            /*
            app.UseConnections(c => c.MapConnectionHandler<MqttConnectionHandler>("/data", options =>
            {
                options.WebSockets.SubProtocolSelector = MQTTnet.AspNetCore.ApplicationBuilderExtensions.SelectSubProtocol;
            }));

            app.UseMqttEndpoint("/data");
            //MQTT声明周期事件
            app.UseMqttServer(server =>
            {
                //开始
                server.StartedHandler = new MqttServerStartedHandlerDelegate(
                    (args) =>
                    {
                        return Task.Run(() =>
                        {
                            Controllers.MqttController.Start();
                        });
                    }
                );

                //停止
                server.StoppedHandler = new MqttServerStoppedHandlerDelegate((args) =>
                {
                    return Task.Run(() =>
                    {
                        
                    });
                });

                server.UseClientConnectedHandler(
                    (args) =>
                    {
                        return Task.Run(() =>
                        {

                        });
                    });
                server.UseClientDisconnectedHandler(
                    (args) =>
                    {
                        return Task.Run(() =>
                        {

                        });
                    });
            });
            app.UseMqttEndpoint();
            */
            #endregion

            app.UseStaticFiles();
            app.UseFileServer();
            app.UseAuthentication();
            app.UseCors("*");
            app.ConfigureCustomExceptionMiddleware();

            var serviceProvider = app.ApplicationServices;
            var httpContextAccessor = serviceProvider.GetRequiredService<IHttpContextAccessor>();
            AuthContextService.Configure(httpContextAccessor);

            app.UseMvc(routes =>
            {

                routes.MapRoute(
                     name: "areaRoute",
                     template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "apiDefault",
                    template: "api/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSwagger(o =>
            {
                o.PreSerializeFilters.Add((document, request) =>
                {
                    document.Paths = document.Paths.ToDictionary(p => p.Key.ToLowerInvariant(), p => p.Value);
                });
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "RBAC API V1");
                //c.RoutePrefix = "";
            });


        }
    }
}
