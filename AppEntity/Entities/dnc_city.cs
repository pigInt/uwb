﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace AppEntity.Entities
{
    /// <summary>
    /// dnc_city:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    public class dnc_city
    {
        /// <summary>
        /// auto_increment
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int zid { get; set; }

        /// <summary>
        /// provinceCode
        /// </summary>
        [Column(TypeName = "varchar(20)")]
        public string provincecode { get; set; }

        /// <summary>
        /// citycode
        /// </summary>
        [Column(TypeName = "varchar(20)")]
        public string citycode { get; set; }

        /// <summary>
        /// cityname
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string cityname { get; set; }

        /// <summary>
        /// status
        /// </summary>
        //[Column(TypeName = "int")]
        public int status { get; set; }

        /// <summary>
        /// desc
        /// </summary>
        //[Column(TypeName = "varchar(50)")]
        public string desc { get; set; }

    }
}
