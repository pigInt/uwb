﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppEntity.Entities
{
    public class dnc_device_extend
    {
        /// <summary>
        /// auto_increment
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int dieid { get; set; }

        /// <summary>
        /// provinceCode
        /// </summary>
         public int diid { get; set; }


       [Column(TypeName = "varchar(20)")]
        public string dicode { get; set; }

        /// <summary>
        /// citycode
        /// </summary>
        [Column(TypeName = "varchar(20)")]
        public string monitor_id { get; set; }

        /// <summary>
        /// cityname
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string field_name { get; set; }

        /// <summary>
        /// cityname
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string local_value { get; set; }

        /// <summary>
        /// cityname
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string remote_value { get; set; }
        /// <summary>
        /// status
        /// </summary>
        //[Column(TypeName = "int")]
        public string create_date { get; set; }

        /// <summary>
        /// desc
        /// </summary>
        //[Column(TypeName = "varchar(50)")]
        public string mod_date { get; set; }
    }
}
