﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppEntity.Entities
{
    /// <summary>
    /// 系统配置实体类
    /// </summary>
    public class settings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        /// <summary>
        /// 配置名称
        /// </summary>
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        /// <summary>
        /// 配置值
        /// </summary>
        [Column(TypeName = "varchar(100)")]
        public string Value { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Description { get; set; }

        [Column(TypeName = "varchar(20)")]
        public string Type { get; set; }

        public Status Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IsDeleted IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

    }
}
