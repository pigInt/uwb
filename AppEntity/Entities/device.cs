﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppEntity.Entities
{
    public class device
    {
        /// <summary>
        /// auto_increment
        /// </summary>
        [Key, Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]

        public int id { get; set; }
        /// <summary>
        /// code
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string code { get; set; }

        /// <summary>
        /// name
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string name { get; set; }

        /// <summary>
        /// type
        /// </summary>
        [Column(TypeName = "varchar(2)")]
        public string type { get; set; }

        /// <summary>
        /// model
        /// </summary>
        [Column(TypeName = "varchar(20)")]
        public string model { get; set; }

        /// <summary>
        /// mac_id
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string mac_id { get; set; }

        /// <summary>
		/// position_top
		/// </summary>        
		public decimal position_top { get; set; }

        /// <summary>
        /// position_left
        /// </summary>        
        public decimal position_left { get; set; }

        /// <summary>
        /// position_x
        /// </summary>        
        public decimal position_x { get; set; }

        /// <summary>
        /// position_y
        /// </summary>        
        public decimal position_y { get; set; }

        /// <summary>
        /// position_z
        /// </summary>        
        public decimal position_z { get; set; }

        /// <summary>
        /// status
        /// </summary>
        public Status status { get; set; }

        /// <summary>
        /// create_time
        /// </summary>
        public DateTime create_time { get; set; }

        /// <summary>
        /// modify_time
        /// </summary>
        public DateTime modify_time { get; set; }

        public int? region_guid { get; set; }

        public int? parent_id { get; set; }

        public int sort_num { get; set; }

        public ICollection<device_alarm> device_alarms { get; set; }

    }
}
