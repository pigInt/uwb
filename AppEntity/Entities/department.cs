﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppEntity.Entities
{
    public class department
    {
        /// <summary>
		/// auto_increment
		/// </summary>
		[Key, Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DefaultValue("newid()")]
        public Guid guid { get; set; }

        /// <summary>
        /// name
        /// </summary>
        [Required, Column(TypeName = "varchar(50)")]
        public string name { get; set; }

        /// <summary>
        /// parent_id
        /// </summary>
        public Guid? parent_guid { get; set; }

        public string parent_name { get; set; }

    }
}
