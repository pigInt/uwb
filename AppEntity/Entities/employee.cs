﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppEntity.Entities
{
    public class employee
    {

        /// <summary>
		/// auto_increment
		/// </summary>
		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DefaultValue("newid()")]
        public Guid guid { get; set; }

        /// <summary>
        /// code
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string code { get; set; }
        /// <summary>
        /// name
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string name { get; set; }

        /// <summary>
        /// sex
        /// </summary>
        [Column(TypeName = "varchar(10)")]
        public string sex { get; set; }

        /// <summary>
        /// age
        /// </summary>
        [Column(TypeName = "varchar(10)")]
        public string age { get; set; }

        /// <summary>
        /// position
        /// </summary>
        [Column(TypeName = "varchar(20)")]
        public string position { get; set; }

        /// <summary>
        /// department
        /// </summary>
        [Column(TypeName = "varchar(10)")]
        public string department { get; set; }

        /// <summary>
        /// telephone
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string telephone { get; set; }

        /// <summary>
        /// status
        /// </summary>
        public Status status { get; set; }

        /// <summary>
        /// create_time
        /// </summary>
        public DateTime? create_time { get; set; }

        /// <summary>
        /// modify_time
        /// </summary>
        public DateTime? modify_time { get; set; }

    }
}
