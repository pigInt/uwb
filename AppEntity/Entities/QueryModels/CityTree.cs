﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEntity.Entities.QueryModels
{
    public class CityTree
    {
        public string provinceCode{ get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string citycode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string cityname { get; set; }
    }
}
