﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppEntity.Entities
{
    public class region
    {
        /// <summary>
		/// auto_increment
		/// </summary>
		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int guid { get; set; }

        /// <summary>
        /// name
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string name { get; set; }

        /// <summary>
        /// position
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string position { get; set; }

        /// <summary>
        /// length
        /// </summary>
        public decimal length { get; set; }

        /// <summary>
        /// width
        /// </summary>
        public decimal width { get; set; }

        /// <summary>
        /// height
        /// </summary>
        public decimal height { get; set; }

        /// <summary>
        /// background
        /// </summary>
        [Column(TypeName = "varchar(100)")]
        public string background { get; set; }

         public int background_ratio { get; set; }       
        
        /// <summary>
        /// longitude
        /// </summary>
        [Column(TypeName = "varchar(20)")]
        public string longitude { get; set; }

        /// <summary>
        /// latitude
        /// </summary>
        [Column(TypeName = "varchar(20)")]
        public string latitude { get; set; }

        /// <summary>
        /// parent_id
        /// </summary>
        public int? parent_guid { get; set; }

        public string parent_name { get; set; }
        /// <summary>
        /// create_time
        /// </summary>
        public DateTime? create_time { get; set; }

        /// <summary>
        /// modify_time
        /// </summary>
        public DateTime? modify_time { get; set; }
    }
}
