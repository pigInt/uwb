﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppEntity.Entities
{
    public class device_tag
    {
        /// <summary>
        /// id
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        /// <summary>
        /// unique_id
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string unique_id { get; set; }

        /// <summary>
        /// name
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string name { get; set; }

        /// <summary>
        /// code
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string code { get; set; }

        /// <summary>
        /// status
        /// </summary>
        public Status status { get; set; }

        /// <summary>
        /// type
        /// </summary>
        [Column(TypeName = "varchar(20)")]
        public string type { get; set; }

        /// <summary>
        /// electricity
        /// </summary>
        public int electricity { get; set; }

        /// <summary>
        /// create_time
        /// </summary>
        public DateTime create_time { get; set; }

        /// <summary>
        /// modify_time
        /// </summary>
        public DateTime modify_time { get; set; }
    }
}
