﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static AppEntity.Entities.Enums.CommonEnum;

namespace AppEntity.Entities
{
    public class dnc_device
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DIid { get; set; }
 
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string DICode { get; set; }

        //[Column(TypeName = "varchar(4)")]
        public string remote_code { get; set; }

        //[Column(TypeName = "varchar(8)")]
        public string site_code { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string DIName { get; set; }

        [Column(TypeName = "varchar(20)")]
        public string DItype { get; set; }

        //[Column(TypeName = "varchar(15)")]
        //public string DIX { get; set; }

        //[Column(TypeName = "varchar(15)")]
        //public string DIY { get; set; }

        //[Column(TypeName = "varchar(15)")]
        public string parent_code { get; set; }

        public int FarterID { get; set; }

        //[Column(TypeName = "varchar(50)")]
        //public string device_ip { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string citycode { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string zonecode { get; set; }

        //public int FarterID { get; set; }
        
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int IsDeleted { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ModDate { get; set; }
    }
}
