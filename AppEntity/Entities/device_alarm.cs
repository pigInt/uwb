﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppEntity.Entities
{
    public class device_alarm
    {
        /// <summary>
		/// auto_increment
		/// </summary>
		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        /// <summary>
        /// device_code
        /// </summary>
        public int device_id { get; set; }

        /// <summary>
        /// status
        /// </summary>
        public Status status { get; set; }

        /// <summary>
        /// alarm_time
        /// </summary>
        public DateTime? alarm_time { get; set; }

        /// <summary>
        /// recover_time
        /// </summary>
        public DateTime? recover_time { get; set; }

        /// <summary>
        /// alarm_level
        /// </summary>
        public int alarm_level { get; set; }

        /// <summary>
        /// alarm_num
        /// </summary>
        public int alarm_num { get; set; }

        /// <summary>
        /// 关联表
        /// </summary>
        public device device { get; set; }
    }
}
