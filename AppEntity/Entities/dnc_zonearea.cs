﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppEntity.Entities
{
    public class dnc_zonearea
    {
        /// <summary>
		/// auto_increment
		/// </summary>
		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int aid { get; set; }

        /// <summary>
        /// zonecode
        /// </summary>
        [Column("zonecode")]
        public string zonecode { get; set; }

        /// <summary>
        /// zonename
        /// </summary>
        [Column("zonename")]
        public string zonename { get; set; }

        /// <summary>
        /// zid
        /// </summary>
        [Column("zid")]
        public int zid { get; set; }

        /// <summary>
        /// zonekey
        /// </summary>
        [Column("zonekey")]
        public string zonekey { get; set; }

        /// <summary>
        /// status
        /// </summary>
        [Column("status")]
        public int status { get; set; }

        /// <summary>
        /// desc
        /// </summary>
        [Column("desc")]
        public string desc { get; set; }

    }
}
