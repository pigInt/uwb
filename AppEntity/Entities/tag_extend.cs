﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static AppEntity.Entities.Enums.CommonEnum;
namespace AppEntity.Entities
{
    public class tag_extend
    {
        /// <summary>
		/// auto_increment
		/// </summary>
		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        /// <summary>
        /// unique_id
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string unique_id { get; set; }

        /// <summary>
        /// region_guid
        /// </summary>
        public int region_guid { get; set; }
        /// <summary>
        /// device_id
        /// </summary>
        public int device_id { get; set; }
        /// <summary>
        /// position_top
        /// </summary>
        public decimal position_top { get; set; }

        /// <summary>
        /// position_left
        /// </summary>
        public decimal position_left { get; set; }

        /// <summary>
        /// position_x
        /// </summary>
        public decimal position_x { get; set; }

        /// <summary>
        /// position_y
        /// </summary>
        public decimal position_y { get; set; }

        /// <summary>
        /// position_z
        /// </summary>
        public decimal position_z { get; set; }

        /// <summary>
        /// on update CURRENT_TIMESTAMP
        /// </summary>
        public DateTime create_time { get; set; }

        /// <summary>
        /// on update CURRENT_TIMESTAMP
        /// </summary>
        public DateTime modify_time { get; set; }
    }
}
