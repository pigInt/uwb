# uwb

#### 项目介绍
UWB（Ultra Wide Band ）即超宽带技术，它是一种无载波通信技术，利用纳秒级的非正弦波窄脉冲传输数据，因此其所占的频谱范围很宽。本系统利用DncZeus框架基础上开发的，是基于netcore+vue开发的前后分离的室内定位系统，其中VUE使用IVIEW框架，后台页面视觉效果极佳。

#### 软件架构
软件架构说明


#### 安装教程
1)	开发环境和工具
2)	下载项目＆安装依赖
3)	DncZeus项目结构解析
4)	新建你的页面
5)	DncZeus框架用户动态权限数据流，鉴权，令牌，验证详解
6)	DncZeus前初步分离项目打包/发布/部署及注意事项


#### 使用说明
    由于 DncZeus 考虑到初级.NET 开发者都可以使用，所以后端项目未涉及过多架构和封装(代码逻辑一目了然)，但为了你更好地熟悉和运用 DncZeus，你需要了解：
1)	ASP.NET Core
2)	Vue.js
3)	iView
4)	ASP.NET Core 的知识能确保你可以看懂和了解后端是如何实现和工作的，而 Vue.js 框架则是前端实现的基石，当然 iView 这个基于 Vue.js 的 UI 框架也是必须要了解的，因为 DncZeus 正是基于 iview-admin(iView 的一个后台管理系统示例项目)来实现的前端 UI 交互。

    关于 ASP.NET Core 和 Vue.js 的入门请参考：
1)	ASP.NET Core 官方文档
2)	Vue.js 官方文档
3)	http://iviewui.com


#### 安装工具
1)	Node.js(同时安装 npm 前端包管理工具)
2)	Visual Studio 2017(15.8.8 或者以上版本)
3)	VS Code 或者其他前端开发工具
4)	git 管理工具
5)	MySQL 数据库


#### 产品体验
    地址：http://47.242.95.107:81/
    超级管理员：administrator
    管理员：admin
    密码：111111



#### 特技
1)	ASP.NET Core 3
2)	ASP.NET WebApi Core
3)	JWT 令牌认证
4)	AutoMapper
5)	Entity Framework Core 2.0
6)	.NET Core 依赖注入
7)	Swagger UI
8)	Vue.js(ES6 语法)
9)	iView(基于 Vue.js 的 UI 框架)

